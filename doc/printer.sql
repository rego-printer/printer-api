/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 10.2.13-MariaDB-10.2.13+maria~xenial-log : Database - printer
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`printer` /*!40100 DEFAULT */;

USE `printer`;

/*Table structure for table `pt_permission` */

DROP TABLE IF EXISTS `pt_permission`;

CREATE TABLE `pt_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `code` varchar(128) NOT NULL,
  `type` varchar(128) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='权限码';

/*Table structure for table `pt_printer` */

DROP TABLE IF EXISTS `pt_printer`;

CREATE TABLE `pt_printer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `printer_id` varchar(32) DEFAULT NULL,
  `printer_key` varchar(32) DEFAULT NULL,
  `task_id` varchar(32) DEFAULT NULL,
  `task_data` varchar(128) DEFAULT NULL,
  `status` varchar(32) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `wifi_version` varchar(32) DEFAULT NULL,
  `app_version` varchar(32) DEFAULT NULL,
  `token` varchar(128) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `ts` datetime DEFAULT NULL,
  PRIMARY KEY (`id`,`create_time`),
  UNIQUE KEY `u_printer_id` (`printer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Table structure for table `pt_printer_log` */

DROP TABLE IF EXISTS `pt_printer_log`;

CREATE TABLE `pt_printer_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `printer_id` varchar(32) DEFAULT NULL,
  `user_id` int(32) DEFAULT NULL,
  `type` varchar(32) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `message` varchar(512) DEFAULT NULL,
  `extra` varchar(512) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `task_id` int(11) DEFAULT NULL,
  `status` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_printer_id` (`printer_id`),
  KEY `idx_create_time` (`create_time`),
  KEY `idx_user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Table structure for table `pt_printer_task` */

DROP TABLE IF EXISTS `pt_printer_task`;

CREATE TABLE `pt_printer_task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `printer_id` varchar(32) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `type` varchar(32) NOT NULL,
  `status` varchar(32) NOT NULL,
  `data` varchar(512) DEFAULT NULL,
  `business_id` varchar(64) NOT NULL,
  `notify_url` varchar(128) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_printer_id` (`printer_id`),
  UNIQUE KEY `u_user_business` (`user_id`, `business_id`),
  KEY `idx_create_time` (`create_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `pt_role` */

DROP TABLE IF EXISTS `pt_role`;

CREATE TABLE `pt_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT=' 角色表';

/*Table structure for table `pt_role_permission` */

DROP TABLE IF EXISTS `pt_role_permission`;

CREATE TABLE `pt_role_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色权限码';

/*Table structure for table `pt_user` */

DROP TABLE IF EXISTS `pt_user`;

CREATE TABLE `pt_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `mail` varchar(128) DEFAULT NULL,
  `type` varchar(32) NOT NULL,
  `create_time` datetime NOT NULL,
  `ts` datetime NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `u_usernmae` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户';

/*Table structure for table `pt_user_api_access` */

DROP TABLE IF EXISTS `pt_user_api_access`;

CREATE TABLE `pt_user_api_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `access_key` varchar(128) NOT NULL,
  `access_secret` varchar(128) NOT NULL,
  `access_token` varchar(128) NOT NULL,
  `expire_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `u_user` (`user_id`),
  UNIQUE KEY `u_key_secret` (`access_key`,`access_secret`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户apitoken';

/*Table structure for table `pt_user_log` */

DROP TABLE IF EXISTS `pt_user_log`;

CREATE TABLE `pt_user_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `create_time` datetime NOT NULL,
  `message` varchar(512) DEFAULT NULL,
  `extra` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`user_id`),
  KEY `idx_create_time` (`create_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `pt_user_role` */

DROP TABLE IF EXISTS `pt_user_role`;

CREATE TABLE `pt_user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(1) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户角色表';

/*Table structure for table `pt_user_token` */

DROP TABLE IF EXISTS `pt_user_token`;

CREATE TABLE `pt_user_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `token` varchar(32) NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `u_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户登录token';

DROP TABLE IF EXISTS `pt_config`;

CREATE TABLE `pt_config` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `config_key` varchar(128) NOT NULL DEFAULT '' COMMENT 'key',
  `config_value` varchar(128) NOT NULL DEFAULT '' COMMENT 'value',
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`config_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='配置表';

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
