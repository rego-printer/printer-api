#! /bin/bash

mvn clean package -U -DskipTests -Pprod

scp target/printer-api.jar www@47.93.205.160:/home/www
ssh www@47.93.205.160 "cd /home/www/ && ./cat.sh printer-api"
ssh www@47.93.205.160 "cd /home/www/ && tail -f printer-api.log"