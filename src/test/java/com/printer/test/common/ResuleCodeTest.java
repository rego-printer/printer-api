package com.printer.test.common;

import com.printer.common.ResultCode;
import org.junit.Test;

/**
 * Created by liwei01 on 2018-05-08
 */
public class ResuleCodeTest {

    @Test
    public void codes() {
        ResultCode[] codes = ResultCode.values();

        for (ResultCode code : codes) {
            System.out.println("|" + code.getCode() +"|" + code.getMessage() + "|");
        }
    }
}
