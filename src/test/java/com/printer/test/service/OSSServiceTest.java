package com.printer.test.service;

import com.printer.PrinterApi;
import com.printer.service.OSSService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.annotation.Resource;
import java.io.IOException;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = PrinterApi.class)
@WebAppConfiguration
@Slf4j
public class OSSServiceTest {

    @Resource
    private OSSService ossService;

    @Test
    public void uploadTest() {
        String time = String.valueOf(System.currentTimeMillis());
        ossService.upload(time, "vs3FtrChvs23osHLvs3A67vpwLK4ybvuvPW3ysCyyta7+rfRDQo=");
        System.out.println(time);
    }

    @Test
    public void getObjectTest() throws IOException {
        String result = ossService.getObject("1527057427905");
        log.info("result------------------------------------------------------------");
        log.info(result);
    }
}
