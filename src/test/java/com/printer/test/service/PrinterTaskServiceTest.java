package com.printer.test.service;

import com.printer.PrinterApi;
import com.printer.common.ApiException;
import com.printer.model.UserInfo;
import com.printer.service.PrinterTaskService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Created by liwei01 on 2018-04-17
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = PrinterApi.class)
@WebAppConfiguration
@Slf4j
public class PrinterTaskServiceTest {

    @Autowired
    private PrinterTaskService printerTaskService;

    @Test
    public void addTaskTest() {
        boolean r = false;
        try {
            UserInfo userInfo =new UserInfo();
            r = printerTaskService.addTask(userInfo, "2", "222", "sdf", "ss");
        } catch (ApiException e) {
            System.out.println(e);
        }
        Assert.assertTrue(r);
    }
}
