package com.printer.test.service;

import com.printer.PrinterApi;
import com.printer.common.PrinterStatus;
import com.printer.dao.PrinterMapper;
import com.printer.model.Printer;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.annotation.Resource;
import java.util.Date;

/**
 * Created by liwei01 on 2018-04-19
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = PrinterApi.class)
@WebAppConfiguration
@Slf4j
public class PrinterServiceTest {

    @Resource
    private PrinterMapper printerMapper;

    @Test
    public void insertTest() {
        for(int i = 3; i < 3000; i++) {
            Printer printer = new Printer();
            printer.setPrinterId(String.valueOf(i));
            printer.setAgentId(1);
            printer.setUserId(1);
            printer.setWifiVersion("1.0.0");
            printer.setAppVersion("1.0.0");
            printer.setCreateTime(new Date());
            printer.setStatus(PrinterStatus.CREATE.name());
            printer.setIsDel((byte)0);
            printer.setTs(new Date());
            printerMapper.insert(printer);
        }
    }
}
