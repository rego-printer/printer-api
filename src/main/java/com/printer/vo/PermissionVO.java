package com.printer.vo;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * Created by liwei01 on 2018-04-13
 */
@Data
@ApiModel("打印机信息")
public class PermissionVO {

    @ApiModelProperty("打印机id")
    private Integer id;

    @ApiModelProperty("打印机id")
    private String name;

    @ApiModelProperty("打印机id")
    private List<String> codes;

    @ApiModelProperty("打印机id")
    private String type;

    @ApiModelProperty("别名")
    private String alias;

    @ApiModelProperty("打印机id")
    private List<PermissionVO> children;
}
