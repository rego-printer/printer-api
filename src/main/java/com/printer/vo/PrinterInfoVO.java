package com.printer.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@ApiModel("打印机信息")
public class PrinterInfoVO {

    @ApiModelProperty("打印机id")
    private String printerId;

    @ApiModelProperty("打印机名称")
    private String printerName;

    @ApiModelProperty("状态")
    private String status;

    @ApiModelProperty("代理id")
    private Integer agentId;

    @ApiModelProperty("用户id")
    private Integer userId;

    @ApiModelProperty("代理名称")
    private String agentUsername;

    @ApiModelProperty("用户名称")
    private String username;

    @ApiModelProperty("wifi版本")
    private String wifiVersion;

    @ApiModelProperty("app版本")
    private String appVersion;

    @ApiModelProperty("创建时间")
    private Date createTime;
}
