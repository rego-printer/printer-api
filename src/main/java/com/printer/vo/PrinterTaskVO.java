package com.printer.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("打印任务")
public class PrinterTaskVO {

    @ApiModelProperty("打印任务id")
    private Integer id;

    @ApiModelProperty("打印机id")
    private String printerId;

    @ApiModelProperty("创建任务的userId")
    private Integer userId;

    @ApiModelProperty("打印状态")
    private String status;

    @ApiModelProperty("外部id")
    private String businessId;

    @ApiModelProperty("通知地址")
    private String notifyUrl;

    @ApiModelProperty("创建时间")
    private Date createTime;

    @ApiModelProperty("更新时间")
    private Date updateTime;

    @ApiModelProperty("额外信息")
    private String extraData;
}
