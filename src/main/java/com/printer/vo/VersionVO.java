package com.printer.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by liwei01 on 2018-05-21
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ApiModel("版本管理")
public class VersionVO {

    @ApiModelProperty("app版本")
    private String appVersion;

    @ApiModelProperty("wifi版本")
    private String wifiVersion;

    @ApiModelProperty("app地址")
    private String appUrl;

    @ApiModelProperty("app版本")
    private String wifiUrl;

}
