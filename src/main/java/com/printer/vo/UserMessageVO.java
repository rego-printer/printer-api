package com.printer.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Created by liwei01 on 2018-05-18
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ApiModel("用户留言")
public class UserMessageVO {

    @ApiModelProperty("id")
    private Integer id;

    @ApiModelProperty("用户名")
    private String username;

    @ApiModelProperty("用户id")
    private Integer userId;

    @ApiModelProperty("留言")
    private String message;

    @ApiModelProperty("创建时间")
    private Date createTime;
}
