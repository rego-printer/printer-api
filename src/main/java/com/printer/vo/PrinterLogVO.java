package com.printer.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@ApiModel("打印机信息")
public class PrinterLogVO {

    @ApiModelProperty("id")
    private Integer id;

    @ApiModelProperty("打印机id")
    private String printerId;

    @ApiModelProperty("类型")
    private String type;

    @ApiModelProperty("创建时间")
    private Date createTime;

    @ApiModelProperty("消息")
    private String message;

    @ApiModelProperty("额外内容")
    private String extra;

    @ApiModelProperty("状态")
    private String status;
}
