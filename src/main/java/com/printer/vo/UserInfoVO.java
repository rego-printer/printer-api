package com.printer.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ApiModel("用户信息")
public class UserInfoVO {

    @ApiModelProperty("用户id")
    private Integer id;

    @ApiModelProperty("用户名")
    private String username;

    @ApiModelProperty("电话号码")
    private String phone;

    @ApiModelProperty("邮件")
    private String mail;

    @ApiModelProperty("用户类型")
    private String type;

    @ApiModelProperty("token")
    private String token;

    @ApiModelProperty("创建时间")
    private Date createTime;

    @ApiModelProperty("上次登录时间")
    private Date lastLoginTime;

    @ApiModelProperty("api accessKey")
    private String accessKey;

    @ApiModelProperty("api accessSecret")
    private String accessSecret;
}
