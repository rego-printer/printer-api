package com.printer.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by liwei01 on 2018-05-30
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ApiModel("系统配置")
public class ConfigVO {

    @ApiModelProperty("心跳时间")
    private Integer heartbeat;

    @ApiModelProperty("打印超时时间")
    private Integer timeout;

    @ApiModelProperty("打印重试次数")
    private Integer retryTimes;
}
