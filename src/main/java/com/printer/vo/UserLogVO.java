package com.printer.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@ApiModel("用户日志")
public class UserLogVO {

    @ApiModelProperty("用户id")
    private Integer userId;

    @ApiModelProperty("用户名")
    private String username;

    @ApiModelProperty("创建时间")
    private Date createTime;

    @ApiModelProperty("类型")
    private String type;

    @ApiModelProperty("日志内容")
    private String message;

    @ApiModelProperty("操作内容")
    private String extra;
}
