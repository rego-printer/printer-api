package com.printer.dao;

import com.printer.model.UserApiAccess;
import com.printer.model.UserApiAccessExample;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserApiAccessMapper {
    long countByExample(UserApiAccessExample example);

    int deleteByExample(UserApiAccessExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(UserApiAccess record);

    int insertSelective(UserApiAccess record);

    List<UserApiAccess> selectByExample(UserApiAccessExample example);

    UserApiAccess selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") UserApiAccess record, @Param("example") UserApiAccessExample example);

    int updateByExample(@Param("record") UserApiAccess record, @Param("example") UserApiAccessExample example);

    int updateByPrimaryKeySelective(UserApiAccess record);

    int updateByPrimaryKey(UserApiAccess record);
}