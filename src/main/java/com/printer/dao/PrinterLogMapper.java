package com.printer.dao;

import com.printer.model.PrinterLog;
import com.printer.model.PrinterLogExample;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PrinterLogMapper {
    long countByExample(PrinterLogExample example);

    int deleteByExample(PrinterLogExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(PrinterLog record);

    int insertSelective(PrinterLog record);

    List<PrinterLog> selectByExample(PrinterLogExample example);

    PrinterLog selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") PrinterLog record, @Param("example") PrinterLogExample example);

    int updateByExample(@Param("record") PrinterLog record, @Param("example") PrinterLogExample example);

    int updateByPrimaryKeySelective(PrinterLog record);

    int updateByPrimaryKey(PrinterLog record);
}