package com.printer.dao;

import com.printer.model.PrinterTask;
import com.printer.model.PrinterTaskExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PrinterTaskMapper {
    long countByExample(PrinterTaskExample example);

    int deleteByExample(PrinterTaskExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(PrinterTask record);

    int insertSelective(PrinterTask record);

    List<PrinterTask> selectByExample(PrinterTaskExample example);

    PrinterTask selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") PrinterTask record, @Param("example") PrinterTaskExample example);

    int updateByExample(@Param("record") PrinterTask record, @Param("example") PrinterTaskExample example);

    int updateByPrimaryKeySelective(PrinterTask record);

    int updateByPrimaryKey(PrinterTask record);
}