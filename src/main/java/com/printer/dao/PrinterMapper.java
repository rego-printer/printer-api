package com.printer.dao;

import com.printer.model.Printer;
import com.printer.model.PrinterExample;
import com.printer.model.PrinterKey;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PrinterMapper {
    long countByExample(PrinterExample example);

    int deleteByExample(PrinterExample example);

    int deleteByPrimaryKey(PrinterKey key);

    int insert(Printer record);

    int insertSelective(Printer record);

    List<Printer> selectByExample(PrinterExample example);

    Printer selectByPrimaryKey(PrinterKey key);

    int updateByExampleSelective(@Param("record") Printer record, @Param("example") PrinterExample example);

    int updateByExample(@Param("record") Printer record, @Param("example") PrinterExample example);

    int updateByPrimaryKeySelective(Printer record);

    int updateByPrimaryKey(Printer record);
}