package com.printer.dao;

import com.printer.model.UserTypePermission;
import com.printer.model.UserTypePermissionExample;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserTypePermissionMapper {
    long countByExample(UserTypePermissionExample example);

    int deleteByExample(UserTypePermissionExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(UserTypePermission record);

    int insertSelective(UserTypePermission record);

    List<UserTypePermission> selectByExample(UserTypePermissionExample example);

    UserTypePermission selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") UserTypePermission record, @Param("example") UserTypePermissionExample example);

    int updateByExample(@Param("record") UserTypePermission record, @Param("example") UserTypePermissionExample example);

    int updateByPrimaryKeySelective(UserTypePermission record);

    int updateByPrimaryKey(UserTypePermission record);
}