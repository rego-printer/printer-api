package com.printer.service;

import com.printer.common.ApiException;
import com.printer.common.ResultCode;
import com.printer.dao.UserApiAccessMapper;
import com.printer.model.UserApiAccess;
import com.printer.model.UserApiAccessExample;
import com.printer.utils.MD5Utils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
@Slf4j
public class UserApiAccessService {

    @Autowired
    private UserApiAccessMapper userApiAccessMapper;


    public boolean insert(UserApiAccess userApiAccess) {
        return userApiAccessMapper.insert(userApiAccess) > 0;
    }

    /**
     * 获取accessToken
     *
     * @param accessKey
     * @param accessSecret
     * @return
     */
    public String getAccessToken(String accessKey, String accessSecret) throws ApiException {
        UserApiAccessExample example = new UserApiAccessExample();
        example.createCriteria().andAccessKeyEqualTo(accessKey)
                .andAccessSecretEqualTo(accessSecret);
        Optional<UserApiAccess> optional = userApiAccessMapper.selectByExample(example).stream().findFirst();

        if (!optional.isPresent()) {
            log.info("api {} {} not found", accessKey, accessSecret);
            throw new ApiException(ResultCode.ACCESS_USER_ERROR);
        }

        UserApiAccess userApiAccess = optional.get();
        String accessToken = MD5Utils.md5(accessKey + accessSecret + System.currentTimeMillis());
        userApiAccess.setAccessToken(accessToken);
        userApiAccessMapper.updateByPrimaryKey(userApiAccess);
        return accessToken;
    }

    /**
     * 根据accessToken获取用户
     *
     * @param accessToken
     * @return
     * @throws ApiException
     */
    public Integer getUserId(String accessToken) throws ApiException {
        UserApiAccessExample example = new UserApiAccessExample();
        example.createCriteria().andAccessTokenEqualTo(accessToken);
        Optional<UserApiAccess> optional = userApiAccessMapper.selectByExample(example).stream().findFirst();

        if (!optional.isPresent()) {
            log.info("accessToken not found {}", accessToken);
            throw new ApiException(ResultCode.ACCESS_TOKEN_EXPIRE);
        }

        return optional.get().getUserId();
    }

    /**
     * 删除token
     *
     * @param userId
     * @return
     */
    public boolean delete(Integer userId) {
        UserApiAccessExample example = new UserApiAccessExample();
        example.createCriteria().andUserIdEqualTo(userId);
        return userApiAccessMapper.deleteByExample(example) > 0;
    }

    public UserApiAccess getByUserId(Integer userId) {
        UserApiAccessExample example = new UserApiAccessExample();
        example.createCriteria().andUserIdEqualTo(userId);
        Optional<UserApiAccess> optional = userApiAccessMapper.selectByExample(example).stream().findFirst();
        if(optional.isPresent()) {
            return optional.get();
        }

        else {
            UserApiAccess apiAccess = new UserApiAccess();
            apiAccess.setUserId(userId);
            apiAccess.setAccessKey(MD5Utils.md5(userId.toString()));
            apiAccess.setAccessSecret(MD5Utils.md5(userId.toString() + System.currentTimeMillis()));
            apiAccess.setAccessToken(MD5Utils.md5(userId.toString() + System.currentTimeMillis()));
            apiAccess.setExpireTime(new Date());
            userApiAccessMapper.insert(apiAccess);
            return apiAccess;
        }
    }
}
