package com.printer.service;

import com.printer.common.ApiException;
import com.printer.common.PrinterStatus;
import com.printer.dao.PrinterMapper;
import com.printer.model.Printer;
import com.printer.model.PrinterExample;
import com.printer.model.User;
import com.printer.model.UserInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

import static com.printer.common.ResultCode.*;
import static com.printer.common.UserType.*;

/**
 * Created by liwei01 on 2018-04-11
 */
@Service
@Slf4j
public class PrinterService {

    @Autowired
    private PrinterMapper printerMapper;

    @Autowired
    private UserLogService userLogService;

    @Autowired
    private UserService userService;

    /**
     * 添加打印机
     *
     * @param userInfo
     * @param printerId
     * @param userId
     * @param agentId
     * @param wifiVersion
     * @param appVersion
     * @throws ApiException
     */
    @Transactional(rollbackFor = ApiException.class)
    public void add(UserInfo userInfo,
                    String printerId, Integer userId,
                    Integer agentId,
                    String wifiVersion,
                    String appVersion) throws ApiException {
        Printer printer = new Printer();
        printer.setPrinterId(printerId);
        printer.setAppVersion(appVersion);
        printer.setWifiVersion(wifiVersion);
        printer.setStatus(PrinterStatus.CREATE.name());
        printer.setUserId(userId);
        printer.setAgentId(agentId);
        printer.setIsDel((byte) 0);
        printer.setCreateTime(new Date());
        printer.setTs(new Date());

        try {
            printerMapper.insert(printer);
            userLogService.addPrinterAddLog(userInfo.getId(), printerId);
            log.info("user {} create printer {}", userInfo.getId(), printerId);
        } catch (Exception e) {
            log.error("add printer error", e);
            throw new ApiException(PRINTER_REPEAT);
        }
    }

    public void update(UserInfo userInfo,
                       String printerId, Integer userId,
                       Integer agentId,String wifiVersion,
                       String appVersion) throws ApiException {

        Printer printer = getByPrinterId(printerId);
        if (printer == null) {
            throw new ApiException(PRINTER_NOT_EXISTS);
        } else {
            printer.setPrinterId(printerId);
            printer.setAppVersion(appVersion);
            printer.setWifiVersion(wifiVersion);
            printer.setUserId(userId);
            printer.setAgentId(agentId);
            printer.setIsDel((byte) 0);
            printer.setTs(new Date());
        }
        printerMapper.updateByPrimaryKeySelective(printer);
        userLogService.addPrinterUpdateLog(userInfo.getId(), printerId);
        log.info("user {} update printer {}", userInfo.getId(), printerId);
    }

    /**
     * 删除打印机
     *
     * @param userInfo
     * @param printerId
     */
    public void delete(UserInfo userInfo, String printerId) {
        Printer printer = new Printer();
        printer.setIsDel((byte) 1);
        printer.setTs(new Date());

        PrinterExample example = new PrinterExample();
        example.createCriteria().andPrinterIdEqualTo(printerId);
        printerMapper.updateByExampleSelective(printer, example);

        userLogService.addPrinterDeleteLog(userInfo.getId(), printerId);
        log.info("user {} delete printer {} {}", userInfo.getId(), printerId);
    }


    /**
     * 绑定打印机
     *
     * @param userInfo
     * @param userId
     * @param printerId
     * @throws ApiException
     */
    @Transactional(rollbackFor = ApiException.class)
    public void bind(UserInfo userInfo, Integer userId, String printerId) throws ApiException {
        Printer printer = getByPrinterId(printerId);
        if (printer == null || printer.getIsDel() == 1) {
            throw new ApiException(PRINTER_NOT_EXISTS);
        }

        if (printer.getUserId() != null && !printer.getUserId().equals(printer.getAgentId())) {
            throw new ApiException(PRINTER_BIND);
        }

        User user = userService.getUserById(userId);
        if (user == null || user.getIsDel() == 1) {
            throw new ApiException(USER_NOT_EXISTS);
        }

        if (userInfo.getType() == LEVEL_2_AGENT && !userId.equals(userInfo.getId())
                && !user.getParentId().equals(userInfo.getId())) {
            throw new ApiException(PERMISSION_ERROR);
        }

        if (userInfo.getType() == LEVEL_3_AGENT && !userId.equals(userInfo.getId())) {
            throw new ApiException(PERMISSION_ERROR);
        }

        if(!hasPermission(userInfo, printer)) {
            throw new ApiException(PERMISSION_ERROR);
        }

        printer.setUserId(userId);
        printer.setTs(new Date());
        if(StringUtils.equals(LEVEL_2_AGENT.name(), user.getType())) {
            printer.setAgentId(userId);
        }
        printerMapper.updateByPrimaryKeySelective(printer);
        userLogService.addPrinterBindLog(userInfo.getId(), userId.toString(), printerId);

        log.info("user {} unbind printer {} {}", userInfo.getId(), userId, printerId);
    }

    public boolean hasPermission(UserInfo userInfo, Printer printer) {
        if(userInfo.getType() == LEVEL_2_AGENT && !userInfo.getId().equals(printer.getUserId()) &&
                !userInfo.getId().equals(printer.getAgentId())) {
            return false;
        }

        if(userInfo.getType() == LEVEL_3_AGENT && !userInfo.getId().equals(printer.getUserId())) {
            return false;
        }

        return true;
    }

    /**
     * 解绑打印机
     *
     * @param userInfo
     * @param printerId
     * @throws ApiException
     */
    @Transactional(rollbackFor = ApiException.class)
    public void unbind(UserInfo userInfo, String printerId) throws ApiException {
        Printer printer = getByPrinterId(printerId);
        if (printer == null || printer.getIsDel() == 1) {
            throw new ApiException(PRINTER_NOT_EXISTS);
        }

        if (printer.getUserId() == null) {
            throw new ApiException(PRINTER_UNBIND);
        }

        User user = userService.getUserById(printer.getUserId());
        if (userInfo.getType() == LEVEL_2_AGENT && !user.getId().equals(userInfo.getId())
                && !user.getParentId().equals(userInfo.getId())) {
            throw new ApiException(PERMISSION_ERROR);
        }

        if (userInfo.getType() == LEVEL_3_AGENT && !user.getId().equals(userInfo.getId())) {
            throw new ApiException(PERMISSION_ERROR);
        }

        if(userInfo.getType() == NORMAL_ADMIN || userInfo.getType() == SYSTEM_ADMIN) {
            printer.setAgentId(null);
        }


        printer.setUserId(null);
        printer.setTs(new Date());
        printerMapper.updateByPrimaryKey(printer);
        userLogService.addPrinterUnbindLog(userInfo.getId(), printerId);

        log.info("user {} unbind printer {}", userInfo.getId(), printerId);
    }

    /**
     * 获取打印机
     *
     * @param printerId
     * @return
     */
    public Printer getByPrinterId(String printerId) {
        PrinterExample example = new PrinterExample();
        example.createCriteria().andPrinterIdEqualTo(printerId).andIsDelEqualTo((byte) 0);
        List<Printer> printers = printerMapper.selectByExample(example);
        if (printers.size() > 0) {
            return printers.get(0);
        }

        return null;
    }

    public List<Printer> query(Integer userId, Integer page, Integer pageSize) {
        PrinterExample example = new PrinterExample();
        example.setOffset((page - 1) * pageSize);
        example.setRows(pageSize);
        example.createCriteria().andUserIdEqualTo(userId);
        List<Printer> printers = printerMapper.selectByExample(example);
        return printers;
    }

    public Long queryCount(Integer userId) {
        PrinterExample example = new PrinterExample();
        example.createCriteria().andUserIdEqualTo(userId);
        return printerMapper.countByExample(example);
    }

    public List<Printer> query(UserInfo userInfo, Integer userId, Integer agentId, String printerId, Integer page, Integer pageSize) {
        PrinterExample example = buildQuery(userInfo, userId, agentId, printerId);
        example.setOffset((page - 1) * pageSize);
        example.setRows(pageSize);
        return printerMapper.selectByExample(example);
    }

    public Long queryCount(UserInfo userInfo, Integer userId, Integer agentId, String printerId) {
        PrinterExample example = buildQuery(userInfo, userId, agentId, printerId);
        return printerMapper.countByExample(example);
    }

    private PrinterExample buildQuery(UserInfo userInfo, Integer userId, Integer agentId, String printerId) {
        PrinterExample example = new PrinterExample();
        PrinterExample.Criteria criteria = example.createCriteria();
        if (userInfo.getType() == LEVEL_2_AGENT) {
            criteria.andAgentIdEqualTo(userInfo.getId());
        } else if (agentId != null) {
            criteria.andAgentIdEqualTo(agentId);
        }
        if (userId != null) {
            criteria.andUserIdEqualTo(userId);
        }
        if (StringUtils.isNotBlank(printerId)) {
            criteria.andPrinterIdEqualTo(printerId);
        }
        example.setOrderByClause("printer_id asc");
        criteria.andIsDelEqualTo((byte) 0);
        return example;
    }
}
