package com.printer.service;

import com.printer.dao.UserMessageMapper;
import com.printer.model.UserInfo;
import com.printer.model.UserMessage;
import com.printer.model.UserMessageExample;
import com.printer.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * Created by liwei01 on 2018-05-18
 */
@Service
@Slf4j
public class UserMessageService {

    @Resource
    private UserMessageMapper userMessageMapper;

    public boolean insert(UserInfo userInfo, String message) {
        UserMessage userMessage = new UserMessage();
        userMessage.setUserId(userInfo.getId());
        userMessage.setMessage(message);
        userMessage.setCreateTime(new Date());

        try {
            return userMessageMapper.insert(userMessage) > 0;
        } catch (Exception e) {
            log.error("insert message error", e);
        }
        return false;
    }

    public List<UserMessage> query(Integer userId, String startTime, String endTime, Integer page, Integer pageNo) {
        UserMessageExample example = buildQuery(userId, startTime, endTime);
        example.setOffset((page - 1) * pageNo);
        example.setRows(pageNo);
        return userMessageMapper.selectByExample(example);
    }

    public Long queryCount(Integer userId, String startTime, String endTime) {
        UserMessageExample example = buildQuery(userId, startTime, endTime);
        return userMessageMapper.countByExample(example);
    }

    public UserMessageExample buildQuery(Integer userId, String startTime, String endTime) {
        UserMessageExample example = new UserMessageExample();
        example.setOrderByClause("id desc");
        UserMessageExample.Criteria criteria = example.createCriteria();
        if(userId != null) {
            criteria.andUserIdEqualTo(userId);
        }

        if (startTime != null) {
            criteria.andCreateTimeGreaterThanOrEqualTo(DateUtils.parseDay(startTime));
        }
        if (endTime != null) {
            criteria.andCreateTimeLessThanOrEqualTo(DateUtils.parseDay(endTime));
        }

        return example;
    }


}
