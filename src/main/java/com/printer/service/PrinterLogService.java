package com.printer.service;

import com.printer.common.PrintLogType;
import com.printer.dao.PrinterLogMapper;
import com.printer.model.PrinterLog;
import com.printer.model.PrinterLogExample;
import com.printer.utils.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by liwei01 on 2018-04-16
 */
@Service
public class PrinterLogService {

    @Autowired
    private PrinterLogMapper printerLogMapper;

    public List<PrinterLog> query(String printerId,
                                  PrintLogType type,
                                  String startTime,
                                  String endTime,
                                  Integer page,
                                  Integer pageSize) {
        PrinterLogExample example = buildQuery(printerId, type, startTime, endTime);
        example.setOffset((page - 1) * pageSize);
        example.setRows(pageSize);
        return printerLogMapper.selectByExample(example);
    }

    public Long queryCount(String printerId,
                           PrintLogType type,
                           String startTime,
                           String endTime) {
        PrinterLogExample example = buildQuery(printerId, type, startTime, endTime);
        return printerLogMapper.countByExample(example);
    }

    public PrinterLogExample buildQuery(String printerId,
                                        PrintLogType type,
                                        String startTime,
                                        String endTime) {
        PrinterLogExample example = new PrinterLogExample();
        PrinterLogExample.Criteria criteria = example.createCriteria();
        example.setOrderByClause("id desc");

        if (StringUtils.isNotBlank(printerId)) {
            criteria.andPrinterIdEqualTo(printerId);
        }

        if (type != null) {
            criteria.andTypeEqualTo(type.name());
        }

        if (StringUtils.isNotBlank(startTime)) {
            criteria.andCreateTimeGreaterThanOrEqualTo(DateUtils.parseDay(startTime));
        }

        if (StringUtils.isNotBlank(endTime)) {
            criteria.andCreateTimeLessThanOrEqualTo(DateUtils.parseDay(endTime));
        }
        return example;
    }
}
