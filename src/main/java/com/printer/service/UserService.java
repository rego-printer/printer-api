package com.printer.service;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.printer.common.ApiException;
import com.printer.common.UserType;
import com.printer.dao.UserMapper;
import com.printer.model.User;
import com.printer.model.UserApiAccess;
import com.printer.model.UserExample;
import com.printer.model.UserInfo;
import com.printer.utils.MD5Utils;
import com.printer.vo.UserInfoVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static com.printer.common.ResultCode.*;
import static com.printer.utils.Constants.TOKEN_EXPIRE_TIME;

/**
 * Created by liwei01 on 2018-03-27
 */
@Service
@Slf4j
public class UserService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserLogService userLogService;

    @Autowired
    private RedisService redisService;

    @Autowired
    private UserApiAccessService userApiAccessService;

    private Cache<String, String> cache = CacheBuilder.newBuilder()
            .expireAfterWrite(60, TimeUnit.MINUTES).build();

    /**
     * 登录
     *
     * @param username
     * @param password
     * @return
     * @throws ApiException
     */
    public UserInfoVO login(String username, String password) throws ApiException {
        UserExample example = new UserExample();
        example.createCriteria().andUsernameEqualTo(username).andIsDelEqualTo((byte) 0);
        List<User> users = userMapper.selectByExample(example);

        if (users == null || users.size() == 0) {
            throw new ApiException(USER_NOT_EXISTS);
        }

        User user = users.get(0);
        if (!user.getPassword().equalsIgnoreCase(MD5Utils.md5(password))) {
            throw new ApiException(USER_PASSWORD_ERROR);
        }

        String token = MD5Utils.md5(username + user.getId() + System.currentTimeMillis());
        String oldToken = redisService.get(user.getId().toString());
        if (StringUtils.isNotBlank(oldToken)) {
            redisService.del(oldToken);
        }
        redisService.set(token, user.getId().toString());
        redisService.set(user.getId().toString(), token);
        redisService.expire(token, TOKEN_EXPIRE_TIME);
        redisService.expire(user.getId().toString(), TOKEN_EXPIRE_TIME);

        user.setLastLoginTime(new Date());
        userMapper.updateByPrimaryKey(user);

        UserInfoVO userInfoVO = UserInfoVO.builder().id(user.getId())
                .mail(user.getMail())
                .phone(user.getPhone())
                .token(token)
                .username(user.getUsername())
                .createTime(user.getCreateTime())
                .lastLoginTime(user.getLastLoginTime())
                .type(user.getType())
                .build();

        userLogService.addLoginLog(userInfoVO.getId());
        log.info("user userId {} username {} login", user.getId(), user.getUsername());
        return userInfoVO;
    }

    /**
     * 登出
     *
     * @param user
     */
    public void logout(UserInfo user) {
        String token = redisService.get(user.getId().toString());
        if (token != null) {
            redisService.del(token);
        }
        redisService.del(user.getId().toString());
        userLogService.addLogoutLog(user.getId());

        log.info("user userId {} username {} logout", user.getId(), user.getUsername());
    }

    public User getUserById(Integer userId) {
        return userMapper.selectByPrimaryKey(userId);
    }

    /**
     * 添加用户
     *
     * @param user
     * @param username
     * @param password
     * @param phone
     * @param mail
     * @param type
     * @throws ApiException
     */
    @Transactional(rollbackFor = ApiException.class)
    public void addUser(UserInfo user, String username,
                        String password, String phone,
                        String mail, UserType type,
                        Integer parentId) throws ApiException {
        if (user.getType() == UserType.LEVEL_2_AGENT) {
            parentId = user.getId();
            type = UserType.LEVEL_3_AGENT;
        }
        User u = new User();
        u.setUsername(username);
        u.setPassword(MD5Utils.md5(password));
        u.setPhone(phone);
        u.setType(type.name());
        u.setMail(mail);
        u.setCreateTime(new Date());
        u.setParentId(parentId);
        u.setTs(new Date());
        u.setIsDel((byte) 0);
        try {
            userMapper.insert(u);
            userLogService.addUserAddLog(user.getId(), username);
            log.info("user userId {} username {} add user {}", user.getId(), user.getUsername(), username);
        } catch (Exception e) {
            log.error("insert user error", e);
            throw new ApiException(USER_REPEAT);
        }
    }

    /**
     * 修改用户
     * 一级代理只能修改它的子代理和本身信息
     * 二级代理只能修改自己信息
     *
     * @param user
     * @param userId
     * @param username
     * @param password
     * @param phone
     * @param mail
     * @param type
     * @throws ApiException
     */
    @Transactional(rollbackFor = ApiException.class)
    public void updateUser(UserInfo user, Integer userId, String username,
                           String password, String phone,
                           String mail, UserType type) throws ApiException {
        if (user.getType() == UserType.LEVEL_2_AGENT && !user.getId().equals(userId)) {
            User u = getUserById(userId);
            if(u != null || !u.getParentId().equals(user.getId())) {
                throw new ApiException(PERMISSION_ERROR);
            }
        }
        if(user.getType() == UserType.LEVEL_3_AGENT) {
            userId = user.getId();
        }
        User u = new User();
        u.setId(userId);
        u.setUsername(username);
        if(StringUtils.isNotBlank(password)) {
            u.setPassword(MD5Utils.md5(password));
        }
        u.setPhone(phone);
        u.setType(type.name());
        u.setMail(mail);
        u.setTs(new Date());

        try {
            userMapper.updateByPrimaryKeySelective(u);
            userLogService.addUserUpdateLog(user.getId(), username);
            log.info("user userId {} username {} update user {}", user.getId(), user.getUsername(), username);
        } catch (Exception e) {
            log.error("update user error", e);
            throw new ApiException(UNKNOWN_EXCEPTION);
        }
    }

    public UserInfoVO profile(UserInfo userInfo) {
        User user = getUserById(userInfo.getId());
        UserApiAccess apiAccess = userApiAccessService.getByUserId(userInfo.getId());
        UserInfoVO userInfoVO = UserInfoVO.builder().id(user.getId())
                .mail(user.getMail())
                .phone(user.getPhone())
                .username(user.getUsername())
                .createTime(user.getCreateTime())
                .lastLoginTime(user.getLastLoginTime())
                .type(user.getType())
                .accessKey(apiAccess.getAccessKey())
                .accessSecret(apiAccess.getAccessSecret())
                .build();
        return userInfoVO;
    }

    public String getUsername(Integer userId) {
        try {
            String username = cache.get(String.valueOf(userId), () -> {
                User user = getUserById(userId);
                if(user != null)
                    return user.getUsername();
                return null;
            });

            return username;
        } catch (ExecutionException e) {
            log.error("getUsername error", e);
        }

        return null;
    }



    public List<User> query(Integer userId, String username, UserType type, Integer parentId, String phone, String mail, int page, int size) {
        UserExample example = buildExample(userId, username, type, parentId, phone, mail);
        example.setOffset((page - 1) * size);
        example.setRows(size);
        List<User> users = userMapper.selectByExample(example);
        return users;
    }

    public Long queryCount(Integer userId, String username, UserType type, Integer parentId, String phone, String mail) {
        UserExample example = buildExample(userId, username, type, parentId, phone, mail);
        return userMapper.countByExample(example);
    }

    public boolean delete(UserInfo userInfo, Integer userId) {

        Integer parentId = null;
        if (userInfo.getType() == UserType.LEVEL_2_AGENT) {
            parentId = userInfo.getId();
        }

        User user = new User();
        user.setTs(new Date());
        user.setIsDel((byte) 1);

        UserExample example = new UserExample();
        UserExample.Criteria criteria = example.createCriteria().andIdEqualTo(userId);
        if (parentId != null) {
            criteria.andParentIdEqualTo(parentId);
        }
        boolean r = userMapper.updateByExampleSelective(user, example) > 0;
        userLogService.addUserDeleteLog(userInfo.getId(), userId.toString());

        String token = redisService.get(userId.toString());
        if (StringUtils.isNotBlank(token)) {
            redisService.del(token);
        }
        redisService.del(userId.toString());
        userApiAccessService.delete(userId);
        log.info("delete user {} {}", userInfo.getId(), userId);
        return r;
    }

    private UserExample buildExample(Integer userId, String username, UserType type, Integer parentId, String phone, String mail) {
        UserExample example = new UserExample();
        UserExample.Criteria criteria = example.createCriteria().andIsDelEqualTo((byte) 0);
        if (userId != null) {
            criteria.andIdEqualTo(userId);
        }

        if (StringUtils.isNotBlank(username)) {
            criteria.andUsernameLike("%" + username + "%");
        }

        if (type != null) {
            criteria.andTypeEqualTo(type.name());
        }

        if (parentId != null) {
            criteria.andParentIdEqualTo(parentId);
        }

        if (StringUtils.isNotBlank(phone)) {
            criteria.andPhoneEqualTo(phone);
        }

        if (StringUtils.isNotBlank(mail)) {
            criteria.andMailEqualTo(mail);
        }
        return example;
    }

    public List<Integer> children(Integer userId) {
        UserExample example = new UserExample();
        example.createCriteria().andIsDelEqualTo((byte) 0).andParentIdEqualTo(userId);
        List<User> users = userMapper.selectByExample(example);
        return users.stream().map(user -> user.getId()).collect(Collectors.toList());
    }

}
