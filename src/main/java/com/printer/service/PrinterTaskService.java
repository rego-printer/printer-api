package com.printer.service;

import com.printer.common.ApiException;
import com.printer.common.PrinterTaskStatus;
import com.printer.common.PrinterTaskType;
import com.printer.dao.PrinterTaskMapper;
import com.printer.model.Printer;
import com.printer.model.PrinterTask;
import com.printer.model.PrinterTaskExample;
import com.printer.model.UserInfo;
import com.printer.utils.DateUtils;
import com.printer.utils.MD5Utils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static com.printer.common.PrinterTaskStatus.CANCEL;
import static com.printer.common.PrinterTaskStatus.FINISH;
import static com.printer.common.ResultCode.ACCESS_BUSINESS_REPEAT;
import static com.printer.common.ResultCode.PERMISSION_ERROR;
import static com.printer.common.ResultCode.PRINTER_NOT_EXISTS;
import static com.printer.common.UserType.LEVEL_3_AGENT;
import static com.printer.utils.Constants.PRINTER_TASK_MAP;
import static com.printer.utils.Constants.PRINTER_TASK_NOTIFY;

/**
 * 打印任务
 */

@Service
@Slf4j
public class PrinterTaskService {

    @Autowired
    private OSSService ossService;

    @Autowired
    private PrinterTaskMapper printerTaskMapper;

    @Autowired
    private RedisService redisService;

    @Autowired
    private UserLogService userLogService;

    @Autowired
    private PrinterService printerService;

    public boolean addTask(UserInfo userInfo, String printerId, String businessId, String data, String notifyUrl) throws ApiException {
        PrinterTaskExample example = new PrinterTaskExample();
        example.createCriteria().andBusinessIdEqualTo(businessId).andUserIdEqualTo(userInfo.getId());
        List<PrinterTask> tasks = printerTaskMapper.selectByExample(example);
        if (tasks.size() > 0) {
            throw new ApiException(ACCESS_BUSINESS_REPEAT);
        }

        Printer printer = printerService.getByPrinterId(printerId);
        if(printer == null) {
            throw new ApiException(PRINTER_NOT_EXISTS);
        }

        if(!printerService.hasPermission(userInfo, printer)) {
            throw new ApiException(PERMISSION_ERROR);
        }

        PrinterTask printerTask = new PrinterTask();
        printerTask.setPrinterId(printerId);
        printerTask.setUserId(userInfo.getId());
        printerTask.setStatus(PrinterTaskStatus.CREATE.name());
        printerTask.setCreateTime(new Date());
        printerTask.setUpdateTime(new Date());
        printerTask.setType(PrinterTaskType.PRINT.name());
        printerTask.setBusinessId(businessId);
        printerTask.setNotifyUrl(notifyUrl);
        printerTask.setRetryTimes(0);
        String name = MD5Utils.md5(data);
        printerTask.setData(name);

        ossService.upload(name, data);
        printerTaskMapper.insert(printerTask);
        redisService.publish(PRINTER_TASK_NOTIFY, printerTask.getId().toString());
        redisService.hset(PRINTER_TASK_MAP, printerTask.getId().toString(), String.valueOf(System.currentTimeMillis()));
        userLogService.addPrinterTaskAddLog(userInfo.getId(), printerTask.getId());
        log.info("add task userId {} printerId {} businessId {} notifyUrl {}", userInfo.getId(), printerId, businessId, notifyUrl);
        return true;
    }

    public PrinterTask selectByBusinessId(Integer userId, String businessId) {
        PrinterTaskExample example = new PrinterTaskExample();
        example.createCriteria().andBusinessIdEqualTo(businessId).andUserIdEqualTo(userId);
        List<PrinterTask> tasks = printerTaskMapper.selectByExample(example);
        if (tasks.size() > 0) {
            return tasks.get(0);
        }

        return null;
    }

    public PrinterTask selectById(Integer taskId) {
        return printerTaskMapper.selectByPrimaryKey(taskId);
    }

    public boolean cancelTask(Integer userId, String businessId) {
        PrinterTaskExample example = new PrinterTaskExample();
        example.createCriteria().andBusinessIdEqualTo(businessId).andUserIdEqualTo(userId);
        List<PrinterTask> tasks = printerTaskMapper.selectByExample(example);
        if (tasks.size() > 0
                && !tasks.get(0).getStatus().equals(FINISH.name())) {
            PrinterTask printerTask = tasks.get(0);
            printerTask.setStatus(CANCEL.name());
            printerTask.setUpdateTime(new Date());
            boolean r = printerTaskMapper.updateByPrimaryKey(printerTask) > 0;
            if (r) {
                userLogService.addPrinterTaskCancelLog(userId, printerTask.getId());
            }
            return r;
        }

        return false;
    }

    public boolean canceTask(Integer userId, PrinterTask printerTask) {
        if (printerTask != null
                && !printerTask.getStatus().equals(FINISH.name())) {
            printerTask.setStatus(CANCEL.name());
            printerTask.setUpdateTime(new Date());
            boolean r = printerTaskMapper.updateByPrimaryKey(printerTask) > 0;
            if (r) {
                userLogService.addPrinterTaskCancelLog(userId, printerTask.getId());
            }
            return r;
        }
        return false;
    }

    public List<PrinterTask> query(UserInfo userInfo, String printerId,
                                   Integer taskId, String businessId, Integer userId, String startTime,
                                   String endTime, PrinterTaskStatus status, Integer page, Integer pageSize) {
        PrinterTaskExample example = buildQuery(userInfo, printerId, taskId, businessId, userId, startTime, endTime, status);
        example.setOffset((page - 1) * pageSize);
        example.setRows(pageSize);
        return printerTaskMapper.selectByExample(example);
    }

    public Long queryCount(UserInfo userInfo, String printerId,
                           Integer taskId, String businessId, Integer userId, String startTime,
                           String endTime, PrinterTaskStatus status) {
        PrinterTaskExample example = buildQuery(userInfo, printerId, taskId, businessId, userId, startTime, endTime, status);
        return printerTaskMapper.countByExample(example);
    }


    private PrinterTaskExample buildQuery(UserInfo userInfo, String printerId,
                                          Integer taskId, String businessId, Integer userId, String startTime,
                                          String endTime, PrinterTaskStatus status) {


        PrinterTaskExample example = new PrinterTaskExample();
        example.setOrderByClause("id desc");



        PrinterTaskExample.Criteria criteria = example.createCriteria();
        if(userInfo.getType() == LEVEL_3_AGENT) {
            userId = userInfo.getParentId();
            List<Printer> printers = printerService.query(userInfo.getId(), 1, Integer.MAX_VALUE);
            criteria.andPrinterIdIn(printers.stream().map(printer -> printer.getPrinterId()).collect(Collectors.toList()));
        }
        if (userId != null) {
            criteria.andUserIdEqualTo(userId);
        }
        if (taskId != null) {
            criteria.andIdEqualTo(taskId);
        }
        if (StringUtils.isNotBlank(printerId)) {
            criteria.andPrinterIdEqualTo(printerId);
        }
        if(StringUtils.isNotBlank(businessId)) {
            criteria.andBusinessIdEqualTo(businessId);
        }
        if (StringUtils.isNotBlank(startTime)) {
            criteria.andCreateTimeGreaterThanOrEqualTo(DateUtils.parseDay(startTime));
        }

        if (StringUtils.isNotBlank(endTime)) {
            criteria.andCreateTimeLessThanOrEqualTo(DateUtils.parseDay(endTime));
        }

        if(status != null) {
            criteria.andStatusEqualTo(status.name());
        }

        return example;
    }
}
