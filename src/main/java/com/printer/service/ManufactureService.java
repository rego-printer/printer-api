package com.printer.service;

import com.printer.common.ManufactureStatus;
import com.printer.dao.ManufactureMapper;
import com.printer.model.Manufacture;
import com.printer.model.ManufactureExample;
import com.printer.model.UserInfo;
import com.printer.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class ManufactureService {

    @Autowired
    private ManufactureMapper manufactureMapper;

    @Autowired
    private UserLogService userLogService;

    /**
     * 添加生产
     *
     * @param userInfo
     * @param count
     * @param status
     * @param remark
     * @param createTime
     * @param finishTime
     * @return
     */
    public boolean add(UserInfo userInfo, int count, ManufactureStatus status, String remark, String createTime, String finishTime) {
        Manufacture manufacture = new Manufacture();
        manufacture.setUserId(userInfo.getId());
        manufacture.setCount(count);
        manufacture.setStatus(status.name());
        manufacture.setRemark(remark);
        manufacture.setIsDel((byte) 0);
        if (createTime != null) {
            manufacture.setCreateTime(DateUtils.parseDay(createTime));
        }
        if (finishTime != null) {
            manufacture.setFinishTime(DateUtils.parseDay(finishTime));
        }
        boolean r = manufactureMapper.insert(manufacture) > 0;
        userLogService.addManufactureAddLog(userInfo.getId(), manufacture.getCount());
        log.info("添加生产 {} {}", userInfo.getId(), count);
        return r;
    }

    /**
     * 更新生产状态
     *
     * @param userInfo
     * @param id
     * @param status
     * @param createTime
     * @param finishTime
     * @return
     */
    public boolean updateStatus(UserInfo userInfo, Integer id, ManufactureStatus status, String remark, String createTime, String finishTime) {
        Manufacture manufacture = manufactureMapper.selectByPrimaryKey(id);
        if (manufacture == null) {
            return false;
        }
        manufacture.setStatus(status.name());
        if (createTime != null) {
            manufacture.setCreateTime(DateUtils.parseDay(createTime));
        }
        if (finishTime != null) {
            manufacture.setFinishTime(DateUtils.parseDay(finishTime));
        }
        if(StringUtils.isNotBlank(remark)) {
            manufacture.setRemark(remark);
        }
        boolean r = manufactureMapper.updateByPrimaryKeySelective(manufacture) > 0;
        userLogService.addManufactureUpdateLog(userInfo.getId(), status.name());
        log.info("更新生产 {} {} {} {} {} {}", userInfo.getId(), id, status, remark, createTime, finishTime);
        return r;
    }

    public List<Manufacture> query(Integer userId, Integer count, ManufactureStatus status, String createTime, String finishTime, Integer page, Integer pageNo) {
        ManufactureExample example = buildQuery(userId, count, status, createTime, finishTime);
        example.setOffset((page - 1) * pageNo);
        example.setRows(pageNo);
        return manufactureMapper.selectByExample(example);
    }

    public Long queryCount(Integer userId, Integer count, ManufactureStatus status, String createTime, String finishTime) {
        ManufactureExample example = buildQuery(userId, count, status, createTime, finishTime);
        return manufactureMapper.countByExample(example);
    }

    public ManufactureExample buildQuery(Integer userId, Integer count, ManufactureStatus status, String createTime, String finishTime) {
        ManufactureExample example = new ManufactureExample();
        ManufactureExample.Criteria criteria = example.createCriteria().andIsDelEqualTo((byte) 0);
        example.setOrderByClause("id desc");
        if (userId != null) {
            criteria.andUserIdEqualTo(userId);
        }
        if (count != null) {
            criteria.andCountEqualTo(count);
        }
        if(status != null) {
            criteria.andStatusEqualTo(status.name());
        }
        if (createTime != null) {
            criteria.andCreateTimeEqualTo(DateUtils.parseDay(createTime));
        }
        if (finishTime != null) {
            criteria.andFinishTimeEqualTo(DateUtils.parseDay(finishTime));
        }
        return example;
    }

    public boolean delete(UserInfo userInfo, Integer id) {
        Manufacture manufacture = new Manufacture();
        manufacture.setId(id);
        manufacture.setIsDel((byte) 1);
        boolean r = manufactureMapper.updateByPrimaryKeySelective(manufacture) > 0;
        userLogService.addManufactureDeleteLog(userInfo.getId(), id);
        log.info("删除生产 {} {}", userInfo.getId(), id);
        return r;
    }
}
