package com.printer.service;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.printer.dao.ConfigMapper;
import com.printer.model.Config;
import com.printer.model.ConfigExample;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class ConfigService {

    @Autowired
    private ConfigMapper configMapper;

    private Cache<String, String> cache = CacheBuilder.newBuilder()
            .expireAfterWrite(60 * 1000, TimeUnit.MILLISECONDS).build();

    public String get(final String configKey) {
        try {
            return cache.get(configKey, ()-> {
                ConfigExample example = new ConfigExample();
                example.createCriteria().andConfigKeyEqualTo(configKey);
                List<Config> configs = configMapper.selectByExample(example);
                return configs.stream().findFirst().get().getConfigValue();
            });
        } catch (Exception e) {
            log.info("getValue error", e);
        }
        return null;
    }

    public Integer getInt(String configKey, int defaultValue) {
        String value = get(configKey);
        if(value != null) {
            return Integer.valueOf(value);
        }
        return defaultValue;
    }

    public void update(String key, String value) {
        Config config = new Config();
        config.setConfigKey(key);
        config.setConfigValue(value);
        ConfigExample example = new ConfigExample();
        example.createCriteria().andConfigKeyEqualTo(key);
        configMapper.updateByExampleSelective(config, example);
        cache.invalidate(key);
    }
}
