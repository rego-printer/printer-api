package com.printer.service;

import com.alibaba.fastjson.JSON;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.Lists;
import com.printer.common.PermissionCode;
import com.printer.common.PermissionType;
import com.printer.common.UserType;
import com.printer.dao.PermissionMapper;
import com.printer.dao.UserTypePermissionMapper;
import com.printer.model.Permission;
import com.printer.model.PermissionExample;
import com.printer.model.UserTypePermission;
import com.printer.model.UserTypePermissionExample;
import com.printer.vo.PermissionVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Created by liwei01 on 2018-04-12
 */
@Slf4j
@Service
public class PermissionService {

    @Autowired
    private PermissionMapper permissionMapper;

    @Autowired
    private UserTypePermissionMapper userTypePermissionMapper;

    private Cache<UserType, Set<PermissionCode>> userTypePermissions = CacheBuilder
            .newBuilder()
            .expireAfterWrite(600, TimeUnit.MICROSECONDS)
            .build();

    /**
     * 服务权限
     *
     * @param userType
     * @param permissionId
     * @return
     */
    public boolean givePermission(UserType userType, Integer permissionId) {
        UserTypePermissionExample example = new UserTypePermissionExample();
        example.createCriteria().andUserTypeEqualTo(userType.name()).andPermissionIdEqualTo(permissionId);
        long count = userTypePermissionMapper.countByExample(example);
        if (count > 0) {
            return false;
        }

        UserTypePermission permission = new UserTypePermission();
        permission.setPermissionId(permissionId);
        permission.setUserType(userType.name());
        try {
            int r = userTypePermissionMapper.insert(permission);
            return r > 0;
        } catch (Exception e) {
            log.error("givePermission error", e);
        }
        return false;
    }

    /**
     * 删除权限
     *
     * @param userType
     * @param permissionId
     * @return
     */
    public boolean cancelPermission(String userType, Integer permissionId) {
        UserTypePermissionExample example = new UserTypePermissionExample();
        example.createCriteria().andUserTypeEqualTo(userType).andPermissionIdEqualTo(permissionId);
        return userTypePermissionMapper.deleteByExample(example) > 0;
    }

    /**
     * 删除权限
     *
     * @param userType
     * @return
     */
    public boolean cancelPermission(String userType) {
        UserTypePermissionExample example = new UserTypePermissionExample();
        example.createCriteria().andUserTypeEqualTo(userType);
        return userTypePermissionMapper.deleteByExample(example) > 0;
    }

    public List<PermissionVO> findUserPermissionTree(UserType userType) {
        return wrap(findUserPermission(userType));
    }

    private List<Permission> findUserPermission(UserType userType) {
        UserTypePermissionExample example = new UserTypePermissionExample();
        example.createCriteria().andUserTypeEqualTo(userType.name());
        List<UserTypePermission> userTypePermissions = userTypePermissionMapper.selectByExample(example);

        List<Integer> ids = userTypePermissions.stream().map(userTypePermission -> userTypePermission.getPermissionId()).collect(Collectors.toList());

        if (ids.size() == 0) {
            return Lists.newArrayList();
        }
        PermissionExample permissionExample = new PermissionExample();
        permissionExample.createCriteria().andIdIn(ids);
        return permissionMapper.selectByExample(permissionExample);
    }

    public List<PermissionVO> findPermissionTree() {
        PermissionExample permissionExample = new PermissionExample();
        List<Permission> permissions = permissionMapper.selectByExample(permissionExample);
        return wrap(permissions);
    }

    private List<PermissionVO> wrap(List<Permission> permissions) {
        List<PermissionVO> vos = new ArrayList<>();
        for (Permission permission : permissions) {
            if (permission.getType().equals(PermissionType.LEVEL_1_MENU.name())) {
                PermissionVO vo = model2VO(permission);
                vos.add(vo);
                for (Permission p : permissions) {
                    if (permission.getId().equals(p.getParentId())) {
                        vo.getChildren().add(model2VO(p));
                    }
                }
            }
        }
        return vos;
    }

    private PermissionVO model2VO(Permission permission) {
        PermissionVO permissionVO = new PermissionVO();
        permissionVO.setId(permission.getId());
        permissionVO.setName(permission.getName());
        permissionVO.setAlias(permission.getAlias());
        if (StringUtils.isNotBlank(permission.getCodes())) {
            permissionVO.setCodes(JSON.parseArray(permission.getCodes(), String.class));
        }
        if (permission.getType().equals(PermissionType.LEVEL_1_MENU.name())) {
            permissionVO.setChildren(new ArrayList<>());
        }
        return permissionVO;
    }

    /**
     * 赋予所有权限
     *
     * @param userType
     * @param permissionIds
     */
    public void givePermissions(UserType userType, List<Integer> permissionIds) {
        cancelPermission(userType.name());
        for (Integer p : permissionIds) {
            givePermission(userType, p);
        }
    }

    public boolean contains(UserType userType, PermissionCode codes[]) {
        if (codes == null || codes.length == 0) {
            return false;
        }

        try {
            Set<PermissionCode> permissionCodes = userTypePermissions.get(userType, () -> getPermissionCodes(userType));
            for (PermissionCode code : codes) {
                if (permissionCodes.contains(code)) {
                    return true;
                }
            }
        } catch (ExecutionException e) {
            log.info("call back error", e);
        }
        return false;
    }

    private Set<PermissionCode> getPermissionCodes(UserType userType) {
        Set<PermissionCode> permissionCodes = new HashSet<>();
        List<Permission> permissions = findUserPermission(userType);
        permissions.stream().forEach(permission -> {
            if (StringUtils.isNotBlank(permission.getCodes())) {
                List<PermissionCode> codes = JSON.parseArray(permission.getCodes(), PermissionCode.class);
                permissionCodes.addAll(codes);
            }
        });
        return permissionCodes;
    }
}
