package com.printer.service;

import com.alibaba.fastjson.JSON;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.OSSObject;
import com.aliyun.oss.model.PutObjectRequest;
import com.aliyun.oss.model.PutObjectResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

@Service
@Slf4j
public class OSSService implements InitializingBean {

    @Value("${aliyun.oss.endpoint}")
    private String endpoint;

    @Value("${aliyun.oss.accessKeyId}")
    private String accessKeyId;

    @Value("${aliyun.oss.accessKeySecret}")
    private String accessKeySecret;

    @Value("${aliyun.oss.bucketName}")
    private String bucketName;

    private OSS ossClient;

    public void upload(String name, String data) {
        ByteArrayInputStream inputStream = new ByteArrayInputStream(data.getBytes());
        PutObjectResult result = ossClient.putObject(new PutObjectRequest(bucketName, name, inputStream));
        log.info("---------------------------upload result {}", JSON.toJSONString(result));
    }

    public String upload(String name, InputStream inputStream) {
        PutObjectResult result = ossClient.putObject(new PutObjectRequest(bucketName, name, inputStream));
        log.info("---------------------------upload result {}", JSON.toJSONString(result));
        Date expiration = new Date(new Date().getTime() + 3600l * 1000 * 24 * 365 * 10);
        return ossClient.generatePresignedUrl(bucketName, name, expiration).toString();
    }

    public String getObject(String name) throws IOException {
        OSSObject object = ossClient.getObject(bucketName, name);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        InputStream inputStream = object.getObjectContent();

        byte bs[] = new byte[1024];
        int len;
        while ((len = inputStream.read(bs)) > 0) {
            outputStream.write(bs, 0, len);
        }
        return new String(outputStream.toByteArray());
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
        log.info("create oss client");
    }
}
