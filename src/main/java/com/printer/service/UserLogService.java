package com.printer.service;


import com.printer.common.UserLogType;
import com.printer.dao.UserLogMapper;
import com.printer.model.UserLog;
import com.printer.model.UserLogExample;
import com.printer.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

import static com.printer.common.UserLogType.*;

@Service
@Slf4j
public class UserLogService {

    @Autowired
    private UserLogMapper userLogMapper;

    public boolean addLoginLog(Integer userId) {
        return addUserLog(userId, LOGIN, null);
    }

    public boolean addLogoutLog(Integer userId) {
        return addUserLog(userId, LOGOUT, null);
    }

    public boolean addUserAddLog(Integer userId, String username) {
        return addUserLog(userId, USER_ADD, username);
    }

    public boolean addUserUpdateLog(Integer userId, String username) {
        return addUserLog(userId, USER_UPDATE, username);
    }

    public boolean addUserDeleteLog(Integer userId, String deleteUserId) {
        return addUserLog(userId, USER_DELETE, deleteUserId);
    }

    public boolean addPrinterAddLog(Integer userId, String printerId) {
        return addUserLog(userId, PRINTER_ADD, printerId);
    }

    public boolean addPrinterDeleteLog(Integer userId, String printerId) {
        return addUserLog(userId, PRINTER_DELETE, printerId);
    }

    public boolean addPrinterUpdateLog(Integer userId, String printerId) {
        return addUserLog(userId, PRINTER_UPDATE, printerId);
    }

    public boolean addPrinterBindLog(Integer userId, String bindUserId, String printerId) {
        return addUserLog(userId, PRINTER_BIND, "userId: " + bindUserId + " printerId: " + printerId);
    }

    public boolean addPrinterUnbindLog(Integer userId, String printerId) {
        return addUserLog(userId, PRINTER_UNBIND, printerId);
    }

    public boolean addManufactureAddLog(Integer userId, Integer count) {
        return addUserLog(userId, MANUFACTURE_ADD, count.toString());
    }

    public boolean addManufactureUpdateLog(Integer userId, String status) {
        return addUserLog(userId, MANUFACTURE_UPDATE, status);
    }

    public boolean addManufactureDeleteLog(Integer userId, Integer id) {
        return addUserLog(userId, MANUFACTURE_DELETE, id.toString());
    }

    public boolean addPrinterTaskAddLog(Integer userId, Integer id) {
        return addUserLog(userId, PRINTER_TASK_ADD, id.toString());
    }

    public boolean addPrinterTaskCancelLog(Integer userId, Integer id) {
        return addUserLog(userId, PRINTER_TASK_CANCEL, id.toString());
    }

    private boolean addUserLog(Integer userId, UserLogType type, String extra) {
        UserLog userLog = new UserLog();
        userLog.setUserId(userId);
        userLog.setType(type.name());
        userLog.setMessage(type.getMessage());
        userLog.setExtra(extra);
        userLog.setCreateTime(new Date());
        return userLogMapper.insert(userLog) > 0;
    }

    public List<UserLog> query(Integer userId,
                               UserLogType type,
                               String startTime,
                               String endTime,
                               Integer page,
                               Integer pageSize) {
        UserLogExample example = buildQuery(userId, type, startTime, endTime);
        example.setOffset((page - 1) * pageSize);
        example.setRows(pageSize);

        return userLogMapper.selectByExample(example);
    }

    public Long queryCount(Integer userId,
                           UserLogType type,
                           String startTime,
                           String endTime) {
        UserLogExample example = buildQuery(userId, type, startTime, endTime);
        return userLogMapper.countByExample(example);
    }

    private UserLogExample buildQuery(Integer userId,
                                      UserLogType type,
                                      String startTime,
                                      String endTime) {
        UserLogExample example = new UserLogExample();
        example.setOrderByClause("id desc");
        UserLogExample.Criteria criteria = example.createCriteria();
        if (userId != null) {
            criteria.andUserIdEqualTo(userId);
        }

        if (type != null) {
            criteria.andTypeEqualTo(type.name());
        }

        if (startTime != null) {
            criteria.andCreateTimeGreaterThanOrEqualTo(DateUtils.parseDay(startTime));
        }

        if (endTime != null) {
            criteria.andCreateTimeLessThanOrEqualTo(DateUtils.parseDay(endTime));
        }

        return example;
    }


}
