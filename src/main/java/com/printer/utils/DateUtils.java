package com.printer.utils;


import lombok.extern.slf4j.Slf4j;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Slf4j
public class DateUtils {

    public static final String YYYY_MM_DD = "yyyy-MM-dd";

    public static Date parse(String date, String pattern) {
        DateFormat dateFormat = new SimpleDateFormat(pattern);
        try {
            return dateFormat.parse(date);
        } catch (ParseException e) {
            log.error("parse error", e);
        }

        return null;
    }

    public static Date parseDay(String date) {
        return parse(date, YYYY_MM_DD);
    }
}
