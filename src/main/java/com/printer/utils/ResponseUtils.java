package com.printer.utils;

import com.alibaba.fastjson.JSON;
import com.printer.common.APIResponse;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
public class ResponseUtils {

    public static void write(HttpServletResponse response, APIResponse apiResponse) {
        response.setContentType("application/json;charset=UTF-8");
        try {
            response.getWriter().write(JSON.toJSONString(apiResponse));
        } catch (IOException e) {
            log.error("write error", e);
        }
    }
}
