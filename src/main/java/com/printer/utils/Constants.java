package com.printer.utils;

public class Constants {

    public static final String KEY_APP_VERSION = "KEY_APP_VERSION";

    public static final String KEY_APP_URL = "KEY_APP_URL";

    public static final String KEY_WIFI_VERSION = "KEY_WIFI_VERSION";

    public static final String KEY_WIFI_URL = "KEY_WIFI_URL";

    public static final String PRINTER_TASK_NOTIFY = "PRINTER_TASK_NOTIFY";

    public static final String PRINTER_TASK_MAP = "PRINTER_TASK_MAP";

    public static final Integer TOKEN_EXPIRE_TIME = 3600 * 24 * 7;

    public static final String KEY_HEARTBEAT_TIME = "KEY_HEARTBEAT_TIME";

    public static final String KEY_PRINT_TIMEOUT = "KEY_PRINT_TIMEOUT";

    public static final String KEY_PRINT_RETRY_TIMES = "KEY_PRINT_RETRY_TIMES";
}
