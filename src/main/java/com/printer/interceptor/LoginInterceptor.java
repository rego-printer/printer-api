package com.printer.interceptor;

import com.printer.anno.NoAuthenticationNeeded;
import com.printer.anno.WebApi;
import com.printer.common.APIResponse;
import com.printer.common.ApiException;
import com.printer.common.UserType;
import com.printer.model.User;
import com.printer.model.UserInfo;
import com.printer.service.RedisService;
import com.printer.service.UserApiAccessService;
import com.printer.service.UserService;
import com.printer.utils.ResponseUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.printer.common.ResultCode.UNAUTHENTICATED;

/**
 * Created by liwei01 on 2018-03-26
 */
@Slf4j
public class LoginInterceptor extends HandlerInterceptorAdapter {

    public final static String KEY_LOGIN_USER_TOKEN = "KEY_LOGIN_USER_TOKEN";

    @Autowired
    private UserApiAccessService userApiAccessService;

    @Autowired
    private UserService userService;

    @Autowired
    private RedisService redisService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (!(handler instanceof HandlerMethod)) {
            return true;
        }
        HandlerMethod handlerMethod = (HandlerMethod) handler;

        if (!handlerMethod.getBeanType().getName().startsWith("com.printer")) {
            return true;
        }
        NoAuthenticationNeeded noAuthenticationNeeded = handlerMethod.getMethodAnnotation(NoAuthenticationNeeded.class);
        if (noAuthenticationNeeded == null) {
            noAuthenticationNeeded = handlerMethod.getBeanType().getAnnotation(NoAuthenticationNeeded.class);
        }

        if (noAuthenticationNeeded != null) {
            return true;
        }

        WebApi webApi = handlerMethod.getMethodAnnotation(WebApi.class);
        if (webApi == null) {
            webApi = handlerMethod.getBeanType().getAnnotation(WebApi.class);
        }

        if (webApi != null) {
            return apiHandler(request, response);
        } else {
            return adminHandler(request, response);
        }
    }

    private boolean apiHandler(HttpServletRequest request, HttpServletResponse response) {
        String accessToken = request.getHeader("accessToken");
        try {
            Integer userId = userApiAccessService.getUserId(accessToken);
            User user = userService.getUserById(userId);
            UserInfo userInfo = new UserInfo();
            BeanUtils.copyProperties(user, userInfo, "type");
            userInfo.setType(UserType.valueOf(user.getType()));
            request.setAttribute(KEY_LOGIN_USER_TOKEN, userInfo);
        } catch (ApiException e) {
            log.error("api handler error", e);
            ResponseUtils.write(response, APIResponse.error(e.getResultCode()));
            return false;
        }
        return true;
    }

    private boolean adminHandler(HttpServletRequest request, HttpServletResponse response) {
        String token = request.getHeader("token");
        if (StringUtils.isBlank(token)) {
            ResponseUtils.write(response, APIResponse.error(UNAUTHENTICATED));
            return false;
        }
        String userId = redisService.get(token);
        if (StringUtils.isNotBlank(userId)) {
            User user = userService.getUserById(Integer.valueOf(userId));
            UserInfo userInfo = new UserInfo();
            BeanUtils.copyProperties(user, userInfo, "type");
            userInfo.setType(UserType.valueOf(user.getType()));
            request.setAttribute(KEY_LOGIN_USER_TOKEN, userInfo);
        } else {
            ResponseUtils.write(response, APIResponse.error(UNAUTHENTICATED));
            return false;
        }
        return true;
    }
}
