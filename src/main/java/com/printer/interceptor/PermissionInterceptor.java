package com.printer.interceptor;

import com.printer.anno.Permissions;
import com.printer.common.APIResponse;
import com.printer.common.PermissionCode;
import com.printer.model.UserInfo;
import com.printer.service.PermissionService;
import com.printer.utils.ResponseUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.printer.common.ResultCode.PERMISSION_ERROR;
import static com.printer.common.ResultCode.UNAUTHENTICATED;

/**
 * Created by liwei01 on 2018-04-12
 */
@Slf4j
public class PermissionInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private PermissionService permissionService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (!(handler instanceof HandlerMethod)) {
            return true;
        }
        HandlerMethod handlerMethod = (HandlerMethod) handler;
        Permissions permissions = handlerMethod.getMethodAnnotation(Permissions.class);
        if (permissions == null) {
            permissions = handlerMethod.getBeanType().getAnnotation(Permissions.class);
        }

        if (permissions == null) {
            return true;
        }

        PermissionCode codes[] = permissions.value();
        UserInfo userInfo = (UserInfo) request.getAttribute(LoginInterceptor.KEY_LOGIN_USER_TOKEN);
        if (userInfo == null) {
            ResponseUtils.write(response, APIResponse.error(UNAUTHENTICATED));
            return false;
        }

        if (permissionService.contains(userInfo.getType(), codes)) {
            return true;
        }

        log.info("userId {} no permission {}", userInfo.getId(), request.getRequestURI());
        ResponseUtils.write(response, APIResponse.error(PERMISSION_ERROR));
        return false;
    }
}
