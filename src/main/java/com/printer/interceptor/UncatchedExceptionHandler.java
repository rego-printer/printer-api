/**
 * Copyright 2017 mobike.com. All Rights Reserved.
 */
package com.printer.interceptor;

import com.printer.common.APIResponse;
import com.printer.common.ApiException;
import com.printer.common.ResultCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

/**
 * 未捕获异常处理
 *
 * @author lidehua
 * @since 2017-08-22
 */
@Slf4j
@RestControllerAdvice
public class UncatchedExceptionHandler {

    @ExceptionHandler(value = Throwable.class)
    @ResponseStatus(code = HttpStatus.OK)
    public APIResponse<?> exceptionHandler(Exception exception, HttpServletRequest request) {
        log.error("url" + request.getRequestURI(), exception);
        APIResponse<?> apiResponse;
        if (exception instanceof ApiException) {
            apiResponse = new APIResponse<>(((ApiException) exception).getResultCode());
        } else {
            apiResponse = new APIResponse<>(ResultCode.BAD_REQUEST);
            apiResponse.setDescription(exception.getClass().getName() + ": " + exception.getMessage());
        }

        return apiResponse;
    }
}
