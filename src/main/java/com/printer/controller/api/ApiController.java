package com.printer.controller.api;

import com.printer.anno.NoAuthenticationNeeded;
import com.printer.anno.WebApi;
import com.printer.common.APIResponse;
import com.printer.common.ApiException;
import com.printer.common.Page;
import com.printer.model.Printer;
import com.printer.model.PrinterTask;
import com.printer.model.UserInfo;
import com.printer.service.PrinterService;
import com.printer.service.PrinterTaskService;
import com.printer.service.UserApiAccessService;
import com.printer.vo.PrinterInfoVO;
import com.printer.vo.PrinterTaskVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

import static com.printer.common.ResultCode.ACCESS_TASK_NOT_EXISTS;

@RestController
@RequestMapping("/api")
@Api(value = "api", description = "对外api")
public class ApiController {

    @Autowired
    private UserApiAccessService userApiAccessService;

    @Autowired
    private PrinterTaskService printerTaskService;

    @Autowired
    private PrinterService printerService;

    @ApiOperation(value = "获取访问的accessToken", notes = "0请求成功 1001用户不存在 1002用户密码错误")
    @RequestMapping(value = "/getAccessToken", method = RequestMethod.POST)
    @WebApi
    @NoAuthenticationNeeded
    public APIResponse<String> getAccessToken(@RequestParam("accessKey") String accessKey,
                                              @RequestParam("accessSecret") String accessSecret) throws ApiException {
        String accessToken = userApiAccessService.getAccessToken(accessKey, accessSecret);
        return APIResponse.success(accessToken);
    }

    @ApiOperation(value = "添加打印任务", notes = "1005 没有打印机的权限")
    @RequestMapping(value = "/addPrintTask", method = RequestMethod.POST)
    @WebApi
    public APIResponse addPrintTask(UserInfo userInfo,
                                    @RequestParam("printerId") String printerId,
                                    @RequestParam("businessId") String businessId,
                                    @RequestParam("data") String data,
                                    @RequestParam("notifyUrl") String notifyUrl) throws ApiException {
        printerTaskService.addTask(userInfo, printerId, businessId, data, notifyUrl);
        return APIResponse.success(null);
    }

    @ApiOperation(value = "查询打印任务", notes = "1005 没有打印机的权限")
    @RequestMapping(value = "/searchTask", method = RequestMethod.POST)
    @WebApi
    public APIResponse<PrinterTaskVO> searchTask(UserInfo userInfo,
                                          @RequestParam("businessId") String businessId) {
        PrinterTask printerTask = printerTaskService.selectByBusinessId(userInfo.getId(), businessId);
        if (printerTask != null) {
            PrinterTaskVO vo = new PrinterTaskVO();
            BeanUtils.copyProperties(printerTask, vo);
            return APIResponse.success(vo);
        } else {
            return APIResponse.error(ACCESS_TASK_NOT_EXISTS);
        }
    }

    @ApiOperation(value = "取消打印任务", notes = "1005 没有打印机的权限")
    @RequestMapping(value = "/cancelTask", method = RequestMethod.POST)
    @WebApi
    public APIResponse cancelTask(UserInfo userInfo,
                                  @RequestParam("businessId") String businessId) {
        printerTaskService.cancelTask(userInfo.getId(), businessId);
        return APIResponse.success(null);
    }

    @ApiOperation(value = "api查询打印机列表", notes = "1005 没有打印机的权限")
    @RequestMapping(value = "/queryPrinterList", method = RequestMethod.POST)
    @WebApi
    public APIResponse<Page<PrinterInfoVO>> queryPrinterList(UserInfo userInfo,
                                                             @RequestParam(value = "printerId", required = false) String printerId,
                                                             @RequestParam("page") Integer page,
                                                             @RequestParam("pageSize") Integer pageSize) {
        List<Printer> printers = printerService.query(userInfo, null, null, printerId, page, pageSize);
        int total = printerService.queryCount(userInfo, null, null, null).intValue();
        List<PrinterInfoVO> list = printers.stream().map(printer -> {
            PrinterInfoVO vo = new PrinterInfoVO();
            BeanUtils.copyProperties(printer, vo);
            return vo;
        }).collect(Collectors.toList());
        return APIResponse.page(list, total);
    }


    @ApiOperation(value = "api查询打印任务列表", notes = "1005 没有打印机的权限")
    @RequestMapping(value = "/queryTaskList", method = RequestMethod.POST)
    @WebApi
    public APIResponse<Page<PrinterTaskVO>> queryTaskList(UserInfo userInfo,
                                                          @RequestParam(value = "printerId", required = false) String printerId,
                                                          @RequestParam("page") Integer page,
                                                          @RequestParam("pageSize") Integer pageSize) {
        List<PrinterTask> printerTasks = printerTaskService.query(userInfo, printerId, null, null, userInfo.getId(), null, null, null, page, pageSize);
        int total = printerTaskService.queryCount(userInfo, printerId, null, null, userInfo.getId(), null, null, null).intValue();

        List<PrinterTaskVO> vos = printerTasks.stream().map(printerTask -> {
            PrinterTaskVO vo = new PrinterTaskVO();
            BeanUtils.copyProperties(printerTask, vo);
            return vo;
        }).collect(Collectors.toList());

        return APIResponse.page(vos, total);
    }


}
