package com.printer.controller.admin;

import com.printer.anno.Permissions;
import com.printer.common.APIResponse;
import com.printer.common.ResultCode;
import com.printer.service.ConfigService;
import com.printer.service.OSSService;
import com.printer.vo.VersionVO;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import static com.printer.common.PermissionCode.PRINTER_VERSION;
import static com.printer.utils.Constants.*;

/**
 * Created by liwei01 on 2018-05-21
 */
@Api(value = "版本管理", description = "版本管理")
@RestController
@RequestMapping("/admin/version")
@Slf4j
public class VersionController {

    @Autowired
    private OSSService ossService;

    @Autowired
    private ConfigService configService;


    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    @Permissions(PRINTER_VERSION)
    public APIResponse<String> upload(@RequestParam("file")MultipartFile file) {
        try {
            String url = ossService.upload(file.getOriginalFilename(), file.getInputStream());
            log.info("upload success url = {}", url);
            return APIResponse.success(url);
        } catch (Exception e) {
            log.error("upload error", e);
        }
        return APIResponse.error(ResultCode.UNKNOWN_EXCEPTION);
    }


    @RequestMapping(value = "/info", method = RequestMethod.GET)
    @Permissions(PRINTER_VERSION)
    public APIResponse<VersionVO> info() {
        VersionVO versionVO = VersionVO.builder()
                .appVersion(configService.get(KEY_APP_VERSION))
                .wifiVersion(configService.get(KEY_WIFI_VERSION))
                .appUrl(configService.get(KEY_APP_URL))
                .wifiUrl(configService.get(KEY_WIFI_URL))
                .build();
        return APIResponse.success(versionVO);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @Permissions(PRINTER_VERSION)
    public APIResponse update(@RequestParam("appVersion") String appVersion,
                              @RequestParam("wifiVersion") String wifiVersion,
                              @RequestParam("appUrl") String appUrl,
                              @RequestParam("wifiUrl") String wifiUrl) {
        configService.update(KEY_APP_VERSION, appVersion);
        configService.update(KEY_WIFI_VERSION, wifiVersion);
        configService.update(KEY_APP_URL, appUrl);
        configService.update(KEY_WIFI_URL, wifiUrl);
        return APIResponse.success(null);
    }
}
