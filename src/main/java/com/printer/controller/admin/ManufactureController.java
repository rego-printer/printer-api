package com.printer.controller.admin;

import com.printer.anno.Permissions;
import com.printer.common.APIResponse;
import com.printer.common.ManufactureStatus;
import com.printer.common.Page;
import com.printer.model.Manufacture;
import com.printer.model.UserInfo;
import com.printer.service.ManufactureService;
import com.printer.vo.ManufactureVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

import static com.printer.common.PermissionCode.*;

@Api(value = "生产信息", description = "生产信息")
@RestController
@RequestMapping("/admin/manufacture")
public class ManufactureController {

    @Autowired
    private ManufactureService manufactureService;

    @ApiOperation(value = "添加生产信息", notes = "status=[CREATE,START,FINISH,CANCEL]")
    @Permissions(MANUFACTURE_ADD)
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public APIResponse add(UserInfo userInfo,
                           @RequestParam(value = "count", required = false) Integer count,
                           @RequestParam(value = "status", required = false) ManufactureStatus status,
                           @RequestParam(value = "remark", required = false) String remark,
                           @RequestParam(value = "createTime", required = false) String createTime,
                           @RequestParam(value = "finishTime", required = false) String finishTime) {
        manufactureService.add(userInfo, count, status, remark, createTime, finishTime);
        return APIResponse.OK;
    }

    @ApiOperation(value = "更新生产信息", notes = "status=[CREATE,START,FINISH,CANCEL]")
    @Permissions(MANUFACTURE_UPDATE)
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public APIResponse update(UserInfo userInfo,
                              @RequestParam("id") Integer id,
                              @RequestParam("status") ManufactureStatus status,
                              @RequestParam(value = "remark", required = false) String remark,
                              @RequestParam(value = "createTime", required = false) String createTime,
                              @RequestParam(value = "finishTime", required = false) String finishTime) {
        manufactureService.updateStatus(userInfo, id, status, remark, createTime, finishTime);
        return APIResponse.OK;
    }

    @ApiOperation("查询生产信息")
    @Permissions(MANUFACTURE_QUERY)
    @RequestMapping(value = "/query", method = RequestMethod.GET)
    public APIResponse<Page<ManufactureVO>> query(@RequestParam(value = "userId", required = false) Integer userId,
                                                  @RequestParam(value = "count", required = false) Integer count,
                                                  @RequestParam(value = "status", required = false) ManufactureStatus status,
                                                  @RequestParam(value = "createTime", required = false) String createTime,
                                                  @RequestParam(value = "finishTime", required = false) String finishTime,
                                                  @RequestParam("page") Integer page,
                                                  @RequestParam("pageSize") Integer pageSize) {
        List<Manufacture> manufactures = manufactureService.query(userId, count, status, createTime, finishTime, page, pageSize);
        int total = manufactureService.queryCount(userId, count, status, createTime, finishTime).intValue();
        List<ManufactureVO> vos = manufactures.stream().map(manufacture -> {
            ManufactureVO vo = new ManufactureVO();
            BeanUtils.copyProperties(manufacture, vo);
            return vo;
        }).collect(Collectors.toList());
        return APIResponse.page(vos, total);
    }

    @ApiOperation("更新生产信息")
    @Permissions(MANUFACTURE_DELETE)
    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public APIResponse delete(UserInfo userInfo,
                              @RequestParam("id") Integer id) {
        manufactureService.delete(userInfo, id);
        return APIResponse.success(null);
    }


}
