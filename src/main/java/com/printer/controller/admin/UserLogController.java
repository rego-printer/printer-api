package com.printer.controller.admin;


import com.printer.anno.Permissions;
import com.printer.common.APIResponse;
import com.printer.common.Page;
import com.printer.common.UserLogType;
import com.printer.model.UserInfo;
import com.printer.model.UserLog;
import com.printer.service.UserLogService;
import com.printer.service.UserService;
import com.printer.vo.UserLogVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

import static com.printer.common.PermissionCode.USER_LOG_QUERY;
import static com.printer.common.PermissionCode.USER_LOG_SELF;

@Api(value = "日志管理", description = "日志管理")
@RestController
@RequestMapping("/admin/user/log")
public class UserLogController {

    @Autowired
    private UserLogService userLogService;

    @Autowired
    private UserService userService;

    @ApiOperation("用户日志")
    @Permissions(USER_LOG_QUERY)
    @RequestMapping(value = "/query", method = RequestMethod.GET)
    public APIResponse<Page<UserLogVO>> query(@RequestParam(value = "userId", required = false) Integer userId,
                                              @RequestParam(value = "type", required = false) UserLogType type,
                                              @RequestParam(value = "startTime", required = false) String startTime,
                                              @RequestParam(value = "endTime", required = false) String endTime,
                                              @RequestParam("page") Integer page,
                                              @RequestParam("pageSize") Integer pageSize) {

        List<UserLog> userLogs = userLogService.query(userId, type, startTime, endTime, page, pageSize);
        int total = userLogService.queryCount(userId, type, startTime, endTime).intValue();
        List<UserLogVO> vos = userLogs.stream().map(userLog -> {
            UserLogVO vo = new UserLogVO();
            BeanUtils.copyProperties(userLog, vo);
            vo.setUsername(userService.getUsername(vo.getUserId()));
            return vo;
        }).collect(Collectors.toList());
        return APIResponse.page(vos, total);
    }

    @ApiOperation("我的日志")
    @Permissions(USER_LOG_SELF)
    @RequestMapping(value = "/self", method = RequestMethod.GET)
    public APIResponse<Page<UserLogVO>> self(UserInfo userInfo,
                                             @RequestParam(value = "type", required = false) UserLogType type,
                                             @RequestParam(value = "startTime", required = false) String startTime,
                                             @RequestParam(value = "endTime", required = false) String endTime,
                                             @RequestParam("page") Integer page,
                                             @RequestParam("pageSize") Integer pageSize) {
        return query(userInfo.getId(), type, startTime, endTime, page, pageSize);
    }
}
