package com.printer.controller.admin;

import com.printer.anno.Permissions;
import com.printer.common.*;
import com.printer.model.User;
import com.printer.model.UserInfo;
import com.printer.service.PermissionService;
import com.printer.service.UserApiAccessService;
import com.printer.service.UserService;
import com.printer.vo.PermissionVO;
import com.printer.vo.UserInfoVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

import static com.printer.common.PermissionCode.*;

@Api(value = "用户", description = "用户管理")
@RestController
@RequestMapping("/admin/user/")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private PermissionService permissionService;

    @Autowired
    private UserApiAccessService userApiAccessService;

    @ApiOperation("获取用户菜单")
    @RequestMapping(value = "getMenus", method = RequestMethod.GET)
    public APIResponse<List<PermissionVO>> getMenus(UserInfo userInfo) {
        List<PermissionVO> vos = permissionService.findUserPermissionTree(userInfo.getType());
        return APIResponse.success(vos);
    }

    @ApiOperation("添加用户")
    @RequestMapping(value = "add", method = RequestMethod.POST)
    @Permissions(USER_ADD)
    public APIResponse addUser(UserInfo userInfo,
                               @RequestParam("username") String username,
                               @RequestParam("password") String password,
                               @RequestParam(value = "phone", required = false) String phone,
                               @RequestParam(value = "mail", required = false) String mail,
                               @RequestParam("type") UserType type,
                               @RequestParam(value = "parentId", required = false) Integer parentId) throws ApiException {
        userService.addUser(userInfo, username, password, phone, mail, type, parentId);
        return APIResponse.OK;
    }

    @ApiOperation("更新用户")
    @RequestMapping(value = "update", method = RequestMethod.POST)
    @Permissions(USER_UPDATE)
    public APIResponse updateUser(UserInfo userInfo,
                                  @RequestParam("userId") Integer userId,
                                  @RequestParam(value = "username", required = false) String username,
                                  @RequestParam(value = "password", required = false) String password,
                                  @RequestParam(value = "phone", required = false) String phone,
                                  @RequestParam(value = "mail", required = false) String mail,
                                  @RequestParam(value = "type", required = false) UserType type) throws ApiException {
        userService.updateUser(userInfo, userId, username, password, phone, mail, type);
        return APIResponse.OK;
    }

    @ApiOperation("查询用户")
    @RequestMapping(value = "query", method = RequestMethod.GET)
    @Permissions(USER_QUERY)
    public APIResponse<Page<UserInfoVO>> query(UserInfo userInfo,
                                               @RequestParam(value = "id", required = false) Integer id,
                                               @RequestParam(value = "username", required = false) String username,
                                               @RequestParam(value = "type", required = false) UserType type,
                                               @RequestParam(value = "parentId", required = false) Integer parentId,
                                               @RequestParam(value = "phone", required = false) String phone,
                                               @RequestParam(value = "mail", required = false) String mail,
                                               @RequestParam("page") Integer page,
                                               @RequestParam("pageSize") Integer pageSize) {
        if (userInfo.getType() == UserType.LEVEL_2_AGENT) {
            parentId = userInfo.getId();
        }
        List<User> users = userService.query(id, username, type, parentId, phone, mail, page, pageSize);
        int total = userService.queryCount(id, username, type, parentId, phone, mail).intValue();

        List<UserInfoVO> list = users.stream().map(user ->
                UserInfoVO.builder().id(user.getId())
                        .mail(user.getMail())
                        .phone(user.getPhone())
                        .username(user.getUsername())
                        .createTime(user.getCreateTime())
                        .lastLoginTime(user.getLastLoginTime())
                        .type(user.getType())
                        .build()
        ).collect(Collectors.toList());
        return APIResponse.page(list, total);
    }

    @ApiOperation("删除用户")
    @RequestMapping(value = "delete", method = RequestMethod.GET)
    @Permissions(USER_DELETE)
    public APIResponse delete(UserInfo userInfo, @RequestParam("userId") Integer userId) {
        boolean r = userService.delete(userInfo, userId);
        if (r) {
            return APIResponse.OK;
        } else {
            return APIResponse.error(ResultCode.UNKNOWN_EXCEPTION);
        }
    }

    @ApiOperation("用户个人信息")
    @Permissions(USER_PROFILE)
    @RequestMapping(value = "profile", method = RequestMethod.GET)
    public APIResponse<UserInfoVO> profile(UserInfo userInfo) {
        UserInfoVO vo = userService.profile(userInfo);
        return APIResponse.success(vo);
    }

    @ApiOperation("更新accessKey")
    @Permissions(USER_PROFILE)
    @RequestMapping(value = "updateAccessKey", method = RequestMethod.GET)
    public APIResponse updateAccessKey(UserInfo userInfo) {
        userApiAccessService.delete(userInfo.getId());
        userApiAccessService.getByUserId(userInfo.getId());
        return APIResponse.OK;
    }
}
