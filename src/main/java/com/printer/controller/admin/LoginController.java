package com.printer.controller.admin;

import com.printer.anno.NoAuthenticationNeeded;
import com.printer.common.APIResponse;
import com.printer.common.ApiException;
import com.printer.model.UserInfo;
import com.printer.service.UserService;
import com.printer.vo.UserInfoVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by liwei01 on 2018-03-27
 */
@Api(value = "登录", description = "登录相关")
@RestController
@RequestMapping("/admin/")
public class LoginController {


    @Autowired
    private UserService userService;


    @ApiOperation(value = "登陆", notes = "0请求成功 1001用户不存在 1002用户密码错误")
    @RequestMapping(value = "login", method = RequestMethod.POST)
    @NoAuthenticationNeeded
    public APIResponse<UserInfoVO> login(@RequestParam("username") String username,
                                         @RequestParam("password") String password) throws ApiException {
        UserInfoVO userInfoVO = userService.login(username, password);
        return APIResponse.success(userInfoVO);
    }

    @ApiOperation("登出")
    @RequestMapping(value = "logout", method = RequestMethod.POST)
    public APIResponse logout(UserInfo userInfo) {
        userService.logout(userInfo);
        return APIResponse.success(null);
    }
}
