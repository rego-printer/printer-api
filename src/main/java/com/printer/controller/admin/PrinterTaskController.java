package com.printer.controller.admin;

import com.printer.anno.Permissions;
import com.printer.common.APIResponse;
import com.printer.common.Page;
import com.printer.common.PrinterTaskStatus;
import com.printer.model.PrinterTask;
import com.printer.model.UserInfo;
import com.printer.service.OSSService;
import com.printer.service.PrinterTaskService;
import com.printer.vo.PrinterTaskVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import static com.printer.common.PermissionCode.PRINTER_TASK_QUERY;
import static com.printer.common.ResultCode.ACCESS_TASK_NOT_EXISTS;

/**
 * Created by liwei01 on 2018-04-11
 */
@Api(value = "打印任务", description = "打印任务")
@RestController
@RequestMapping("/admin/printerTask")
public class PrinterTaskController {

    @Autowired
    private PrinterTaskService printerTaskService;

    @Autowired
    private OSSService ossService;

    @ApiOperation("查询打印任务")
    @Permissions(PRINTER_TASK_QUERY)
    @RequestMapping(value = "/query", method = RequestMethod.GET)
    public APIResponse<Page<PrinterTaskVO>> query(UserInfo userInfo,
                                                  @RequestParam(value = "printerId", required = false) String printerId,
                                                  @RequestParam(value = "taskId", required = false) Integer taskId,
                                                  @RequestParam(value = "businessId", required = false) String businessId,
                                                  @RequestParam(value = "userId", required = false) Integer userId,
                                                  @RequestParam(value = "startTime", required = false) String startTime,
                                                  @RequestParam(value = "endTime", required = false) String endTime,
                                                  @RequestParam(value = "status", required = false) PrinterTaskStatus status,
                                                  @RequestParam("page") Integer page,
                                                  @RequestParam("pageSize") Integer pageSize) {
        List<PrinterTask> printerTasks = printerTaskService.query(userInfo, printerId, taskId, businessId, userId, startTime, endTime, status, page, pageSize);
        int total = printerTaskService.queryCount(userInfo, printerId, taskId, businessId, userId, startTime, endTime, status).intValue();

        List<PrinterTaskVO> vos = printerTasks.stream().map(printerTask -> {
            PrinterTaskVO vo = new PrinterTaskVO();
            BeanUtils.copyProperties(printerTask, vo);
            return vo;
        }).collect(Collectors.toList());

        return APIResponse.page(vos, total);
    }


    @ApiOperation("查询打印数据")
    @Permissions(PRINTER_TASK_QUERY)
    @RequestMapping(value = "/queryData", method = RequestMethod.GET)
    public APIResponse<String> queryData(@RequestParam(value = "taskId", required = false) Integer taskId) throws IOException {

        PrinterTask printerTask = printerTaskService.selectById(taskId);
        if(printerTask == null) {
            return APIResponse.error(ACCESS_TASK_NOT_EXISTS);
        }

        String data = ossService.getObject(printerTask.getData());
        return APIResponse.success(data);
    }

    @ApiOperation("取消打印任务")
    @RequestMapping(value = "/cancel", method = RequestMethod.POST)
    @Permissions(PRINTER_TASK_QUERY)
    public APIResponse cancelTask(UserInfo userInfo,
                                  @RequestParam("taskId") Integer taskId) {
        List<PrinterTask> tasks = printerTaskService.query(userInfo, null, taskId, null, null, null, null, null, 1, 1);
        if(tasks.size() > 0) {
            printerTaskService.canceTask(userInfo.getId(), tasks.get(0));
        }
        return APIResponse.success(null);
    }

    @ApiOperation("批量取消任务")
    @RequestMapping(value = "/batchCancel", method = RequestMethod.POST)
    @Permissions(PRINTER_TASK_QUERY)
    public APIResponse batchCancel(UserInfo userInfo,
                                  @RequestParam("taskIds") String taskIds) {
        String ts[] = taskIds.split(",");
        for(String taskId : ts) {
            List<PrinterTask> tasks = printerTaskService.query(userInfo, null, Integer.valueOf(taskId), null, null, null, null, null, 1, 1);
            if(tasks.size() > 0) {
                printerTaskService.canceTask(userInfo.getId(), tasks.get(0));
            }
        }
        return APIResponse.success(null);
    }
}
