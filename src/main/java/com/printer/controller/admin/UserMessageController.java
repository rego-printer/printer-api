package com.printer.controller.admin;

import com.printer.anno.Permissions;
import com.printer.common.APIResponse;
import com.printer.common.Page;
import com.printer.model.UserInfo;
import com.printer.model.UserMessage;
import com.printer.service.UserMessageService;
import com.printer.service.UserService;
import com.printer.vo.UserMessageVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

import static com.printer.common.PermissionCode.USER_MESSAGE;

/**
 * Created by liwei01 on 2018-05-18
 */
@Api(value = "用户留言", description = "用户留言")
@RestController
@RequestMapping("/admin/userMessage")
public class UserMessageController {

    @Autowired
    private UserMessageService userMessageService;

    @Autowired
    private UserService userService;


    @ApiOperation("添加留言")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public APIResponse add(UserInfo userInfo,
                           @RequestParam("message") String message) {
        userMessageService.insert(userInfo, message);
        return APIResponse.OK;
    }

    @ApiOperation("查询留言")
    @Permissions(USER_MESSAGE)
    @RequestMapping(value = "/query", method = RequestMethod.GET)
    public APIResponse<Page<UserMessageVO>> query(@RequestParam(value = "userId", required = false) Integer userId,
                                                  @RequestParam(value = "startTime", required = false) String startTime,
                                                  @RequestParam(value = "endTime", required = false) String endTime,
                                                  @RequestParam("page") Integer page,
                                                  @RequestParam("pageSize") Integer pageSize) {
        List<UserMessage> messages = userMessageService.query(userId, startTime, endTime, page, pageSize);
        int total = userMessageService.queryCount(userId, startTime, endTime).intValue();
        List<UserMessageVO> vos = messages.stream().map(userMessage -> {
            UserMessageVO vo = new UserMessageVO();
            BeanUtils.copyProperties(userMessage, vo);
            vo.setUsername(userService.getUsername(vo.getUserId()));
            return vo;
        }).collect(Collectors.toList());
        return APIResponse.page(vos, total);
    }
}
