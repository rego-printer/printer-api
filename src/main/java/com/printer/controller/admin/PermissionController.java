package com.printer.controller.admin;

import com.printer.anno.Permissions;
import com.printer.common.APIResponse;
import com.printer.common.UserType;
import com.printer.service.PermissionService;
import com.printer.vo.PermissionVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.printer.common.PermissionCode.PERMISSION_GIVE;
import static com.printer.common.PermissionCode.PERMISSION_QUERY;

/**
 * Created by liwei01 on 2018-04-12
 */
@Api(value = "权限管理", description = "权限管理")
@RestController
@RequestMapping("/admin/permission")
public class PermissionController {

    @Autowired
    private PermissionService permissionService;

    @ApiOperation("查询权限")
    @Permissions(PERMISSION_QUERY)
    @RequestMapping(value = "/query", method = RequestMethod.GET)
    public APIResponse<List<PermissionVO>> query(@RequestParam("userType") UserType userType) {
        List<PermissionVO> vos = permissionService.findUserPermissionTree(userType);
        return APIResponse.success(vos);
    }

    @ApiOperation("赋予权限")
    @Permissions(PERMISSION_GIVE)
    @RequestMapping(value = "/give", method = RequestMethod.POST)
    public APIResponse give(@RequestParam("userType") UserType userType, @RequestParam("permissionId") List<Integer> permissionIds) {
        permissionService.givePermissions(userType, permissionIds);
        return APIResponse.success(null);
    }

    @ApiOperation("查询所有权限")
    @Permissions(PERMISSION_QUERY)
    @RequestMapping(value = "/queryAll", method = RequestMethod.GET)
    public APIResponse<List<PermissionVO>> queryAll() {
        List<PermissionVO> vos = permissionService.findPermissionTree();
        return APIResponse.success(vos);
    }
}
