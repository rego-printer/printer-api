package com.printer.controller.admin;

import com.printer.anno.Permissions;
import com.printer.common.APIResponse;
import com.printer.service.ConfigService;
import com.printer.vo.ConfigVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static com.printer.common.PermissionCode.PRINTER_CONFIG;
import static com.printer.utils.Constants.KEY_HEARTBEAT_TIME;
import static com.printer.utils.Constants.KEY_PRINT_RETRY_TIMES;
import static com.printer.utils.Constants.KEY_PRINT_TIMEOUT;

/**
 * Created by liwei01 on 2018-05-29
 */
@Api(value = "配置管理", description = "配置管理")
@RestController
@RequestMapping("/admin/config")
@Slf4j
public class ConfigController {

    @Autowired
    private ConfigService configService;

    @ApiOperation("获取配置信息")
    @Permissions(PRINTER_CONFIG)
    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public APIResponse<ConfigVO> get() {
        ConfigVO configVO = ConfigVO.builder()
                .heartbeat(configService.getInt(KEY_HEARTBEAT_TIME, 300))
                .timeout(configService.getInt(KEY_PRINT_TIMEOUT, 300))
                .retryTimes(configService.getInt(KEY_PRINT_RETRY_TIMES, 3)).build();
        return APIResponse.success(configVO);
    }

    @ApiOperation("保存配置信息")
    @Permissions(PRINTER_CONFIG)
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public APIResponse save(@RequestParam("heartbeat") String heartbeat,
                            @RequestParam("timeout") String timeout,
                            @RequestParam("retryTimes") String retryTimes) {
        configService.update(KEY_HEARTBEAT_TIME, heartbeat);
        configService.update(KEY_PRINT_TIMEOUT, timeout);
        configService.update(KEY_PRINT_RETRY_TIMES, retryTimes);
        return APIResponse.success(null);
    }


}
