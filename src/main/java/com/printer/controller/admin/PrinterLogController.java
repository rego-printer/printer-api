package com.printer.controller.admin;

import com.printer.anno.Permissions;
import com.printer.common.APIResponse;
import com.printer.common.Page;
import com.printer.common.PrintLogType;
import com.printer.model.PrinterLog;
import com.printer.service.PrinterLogService;
import com.printer.vo.PrinterLogVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

import static com.printer.common.PermissionCode.PRINTER_LOG_QUERY;

@Api(value = "打印机日志", description = "打印机日志")
@RestController
@RequestMapping("/admin/printerLog")
public class PrinterLogController {

    @Autowired
    private PrinterLogService printerLogService;

    @ApiOperation("查询打印日志")
    @Permissions(PRINTER_LOG_QUERY)
    @RequestMapping(value = "/query", method = RequestMethod.GET)
    public APIResponse<Page<PrinterLogVO>> query(@RequestParam(value = "printerId", required = false) String printerId,
                                                 @RequestParam(value = "type", required = false) PrintLogType type,
                                                 @RequestParam(value = "startTime", required = false) String startTime,
                                                 @RequestParam(value = "endTime", required = false) String endTime,
                                                 @RequestParam("page") Integer page,
                                                 @RequestParam("pageSize") Integer pageSize) {
        List<PrinterLog> printerLogs = printerLogService.query(printerId, type, startTime, endTime, page, pageSize);
        int total = printerLogService.queryCount(printerId, type, startTime, endTime).intValue();
        List<PrinterLogVO> vos = printerLogs.stream().map(printerLog -> {
            PrinterLogVO vo = new PrinterLogVO();
            BeanUtils.copyProperties(printerLog, vo);
            return vo;
        }).collect(Collectors.toList());
        return APIResponse.page(vos, total);
    }
}
