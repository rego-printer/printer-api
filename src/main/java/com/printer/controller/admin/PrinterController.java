package com.printer.controller.admin;

import com.printer.anno.Permissions;
import com.printer.common.APIResponse;
import com.printer.common.ApiException;
import com.printer.common.Page;
import com.printer.model.Printer;
import com.printer.model.UserInfo;
import com.printer.service.PrinterService;
import com.printer.service.UserService;
import com.printer.vo.PrinterInfoVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

import static com.printer.common.PermissionCode.*;

@Api(value = "打印机信息", description = "打印机信息")
@RestController
@RequestMapping("/admin/printer")
public class PrinterController {

    @Autowired
    private PrinterService printerService;

    @Autowired
    private UserService userService;

    @ApiOperation("查询打印机")
    @Permissions(PRINTER_QUERY)
    @RequestMapping(value = "/query", method = RequestMethod.GET)
    public APIResponse<Page<PrinterInfoVO>> query(UserInfo user,
                                                  @RequestParam(value = "printerId", required = false) String printerId,
                                                  @RequestParam(value = "userId", required = false) Integer userId,
                                                  @RequestParam(value = "agentId", required = false) Integer agentId,
                                                  @RequestParam("page") Integer page,
                                                  @RequestParam("pageSize") Integer pageSize) throws ApiException {
        List<Printer> printers = printerService.query(user, userId, agentId, printerId, page, pageSize);
        int total = printerService.queryCount(user, userId, agentId, printerId).intValue();
        List<PrinterInfoVO> list = printers.stream().map(printer -> {
            PrinterInfoVO vo = new PrinterInfoVO();
            BeanUtils.copyProperties(printer, vo);
            if(printer.getAgentId() != null) {
                String username = userService.getUsername(printer.getAgentId());
                vo.setAgentUsername(username);
            }
            if(printer.getUserId() != null) {
                String username = userService.getUsername(printer.getUserId());
                vo.setUsername(username);
            }
            return vo;
        }).collect(Collectors.toList());
        return APIResponse.page(list, total);
    }

    @ApiOperation("我的打印机")
    @Permissions(PRINTER_SELF)
    @RequestMapping(value = "/self", method = RequestMethod.GET)
    public APIResponse<Page<PrinterInfoVO>> self(UserInfo user,
                                                 @RequestParam("page") Integer page,
                                                 @RequestParam("pageSize") Integer pageSize) {
        List<Printer> printers = printerService.query(user.getId(), page, pageSize);
        int total = printerService.queryCount(user.getId()).intValue();
        List<PrinterInfoVO> list = printers.stream().map(printer -> {
            PrinterInfoVO vo = new PrinterInfoVO();
            BeanUtils.copyProperties(printer, vo);
            if(printer.getAgentId() != null) {
                String username = userService.getUsername(printer.getAgentId());
                vo.setAgentUsername(username);
            }
            if(printer.getUserId() != null) {
                String username = userService.getUsername(printer.getUserId());
                vo.setUsername(username);
            }
            return vo;
        }).collect(Collectors.toList());
        return APIResponse.page(list, total);
    }

    @ApiOperation("添加打印机")
    @Permissions(PRINTER_ADD)
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public APIResponse add(UserInfo user,
                           @RequestParam("printerId") String printerId,
                           @RequestParam(value = "userId", required = false) Integer userId,
                           @RequestParam(value = "agentId", required = false) Integer agentId,
                           @RequestParam("wifiVersion") String wifiVersion,
                           @RequestParam("appVersion") String appVersion) throws ApiException {
        printerService.add(user, printerId, userId, agentId, wifiVersion, appVersion);
        return APIResponse.OK;
    }

    @ApiOperation("批量增加打印机")
    @Permissions(PRINTER_ADD)
    @RequestMapping(value = "/addPrinters", method = RequestMethod.POST)
    public APIResponse addPrinters(UserInfo user,
                           @RequestParam("printerIds") String printerIds) throws ApiException {
        String ps[] = printerIds.split(",");
        for (String printerId : ps) {
            printerService.add(user, printerId, null, null, "1.0.0", "1.0.0");
        }
        return APIResponse.OK;
    }

    @ApiOperation("修改打印机")
    @Permissions(PRINTER_UPDATE)
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public APIResponse update(UserInfo user,
                              @RequestParam("printerId") String printerId,
                              @RequestParam(value = "userId", required = false) Integer userId,
                              @RequestParam(value = "agentId", required = false) Integer agentId,
                              @RequestParam("wifiVersion") String wifiVersion,
                              @RequestParam("appVersion") String appVersion) throws ApiException {
        printerService.update(user, printerId, userId, agentId, wifiVersion, appVersion);
        return APIResponse.OK;
    }

    @ApiOperation("删除打印机")
    @Permissions(PRINTER_DELETE)
    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public APIResponse delete(UserInfo user,
                              @RequestParam("printerId") String printerId) throws ApiException {
        printerService.delete(user, printerId);
        return APIResponse.OK;
    }

    @ApiOperation("绑定打印机")
    @Permissions(PRINTER_BIND)
    @RequestMapping(value = "/bind", method = RequestMethod.GET)
    public APIResponse bind(UserInfo user,
                            @RequestParam(value = "userId", required = false) Integer userId,
                            @RequestParam("printerIds") String printerIds) throws ApiException {
        String ps[] = printerIds.split(",");
        for (String printerId : ps) {
            printerService.bind(user, userId, printerId);
        }
        return APIResponse.OK;
    }

    @ApiOperation("解除绑定")
    @Permissions(PRINTER_UNBIND)
    @RequestMapping(value = "/unbind", method = RequestMethod.GET)
    public APIResponse unbind(UserInfo user,
                              @RequestParam("printerId") String printerId) throws ApiException {
        printerService.unbind(user, printerId);
        return APIResponse.OK;
    }


}
