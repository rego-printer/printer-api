package com.printer.config;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import redis.clients.jedis.JedisPool;

@Configuration
public class RedisConfig {

    @Value("${reids.host}")
    private String host;

    @Value("${reids.port}")
    private int port;

    @Value("${redis.timeout}")
    private int timeout;

    @Value("${reids.password}")
    private String password;

    @Bean
    public JedisPool jedisPool() {
        GenericObjectPoolConfig config = new GenericObjectPoolConfig();
        return new JedisPool(config, host, port, timeout, password);
    }
}
