package com.printer.config;

import com.google.common.collect.Lists;
import com.printer.model.UserInfo;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.List;

import static springfox.documentation.builders.RequestHandlerSelectors.basePackage;

/**
 * 初始化 Swagger 相关配置
 *
 * @author xieenlong
 * @see /swagger-ui.html
 */
@Configuration
@EnableSwagger2
@Profile("!prod")
public class SwaggerConfiguration {

    @Bean
    public Docket docketAdminApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("Printer Admin Api")
                .select()
                .apis(basePackage("com.printer.controller.admin"))
                .build()
                .apiInfo(adminApiInfo())
                .ignoredParameterTypes(UserInfo.class)
                .globalOperationParameters(commonParameters());
    }

    private ApiInfo adminApiInfo() {
        return new ApiInfoBuilder()
                .title("Printer Admin Api")
                .description("登录后返回token,以后所有接口的请求中头都带token")
                .version("1.0.0")
                .build();
    }

    private List<Parameter> commonParameters() {
        List<Parameter> parameters = Lists.newArrayList();
        parameters.add(new ParameterBuilder()
                .name("token")
                .description("用户token")
                .modelRef(new ModelRef("string"))
                .parameterType("header")
                .required(false)
                .defaultValue("224603d7c11e446ab6907bbf20d20472")
                .build());
        return parameters;
    }

    @Bean
    public Docket docketWebApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("Printer Web Api")
                .select()
                .apis(basePackage("com.printer.controller.api"))
                .build()
                .apiInfo(webApiInfo())
                .ignoredParameterTypes(UserInfo.class)
                .globalOperationParameters(webApiParameters());
    }

    private ApiInfo webApiInfo() {
        return new ApiInfoBuilder()
                .title("Printer Web Api")
                .description("登录后返回accessToken,以后所有接口的请求中头都带accessToken")
                .version("1.0.0")
                .build();
    }

    private List<Parameter> webApiParameters() {
        List<Parameter> parameters = Lists.newArrayList();
        parameters.add(new ParameterBuilder()
                .name("accessToken")
                .description("用户accessToken")
                .modelRef(new ModelRef("string"))
                .parameterType("header")
                .required(false)
                .defaultValue("3432795f41565dc7a607a888a1a7d54c")
                .build());
        return parameters;
    }
}
