package com.printer.config;

import com.printer.interceptor.LoginInterceptor;
import com.printer.interceptor.PermissionInterceptor;
import com.printer.interceptor.UserTokenArgumentResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.List;

/**
 * 初始化拦截器和参数注入
 *
 * @author xieenlong
 */
@Configuration
public class WebMvcConfiguration extends WebMvcConfigurerAdapter {

    private static final String SWAGGER_RESOURCES = "/swagger-resources/**";

    private static final String SWAGGER_ENDPOINT = "/v2/api-docs";

    @Autowired
    private LoginInterceptor loginInterceptor;

    @Autowired
    private PermissionInterceptor permissionInterceptor;

    @Autowired
    private UserTokenArgumentResolver userTokenArgumentResolver;

    @Bean
    public LoginInterceptor loginInterceptor() {
        return new LoginInterceptor();
    }

    @Bean
    public PermissionInterceptor permissionInterceptor() {
        return new PermissionInterceptor();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(loginInterceptor)
                .excludePathPatterns(SWAGGER_RESOURCES)
                .excludePathPatterns(SWAGGER_ENDPOINT);
        registry.addInterceptor(permissionInterceptor)
                .excludePathPatterns(SWAGGER_RESOURCES)
                .excludePathPatterns(SWAGGER_ENDPOINT);
        super.addInterceptors(registry);
    }

    @Bean
    public UserTokenArgumentResolver userTokenArgumentResolver() {
        return new UserTokenArgumentResolver();
    }

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(userTokenArgumentResolver);
        super.addArgumentResolvers(argumentResolvers);
    }
}
