package com.printer.config;

import com.alibaba.druid.pool.DruidDataSource;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.annotation.PreDestroy;
import javax.sql.DataSource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by liwei01 on 2018-03-20
 */
@Configuration
@MapperScan(basePackages = {"com.printer.dao"})
@Slf4j
public class DAOConfig {

    private DruidDataSource dataSource;

    private DataSourceTransactionManager transactionManager;

    @Value("${printer.datasource.username}")
    private String username;

    @Value("${printer.datasource.password}")
    private String password;

    @Value("${printer.datasource.url}")
    private String url;

    private String mapperLocation = "classpath:com/printer/mapper/*.xml";

    private int minIdle = 1;

    private int maxActive = 50;

    private int maxWait = 10000;

    private int timeBetweenEvictionRunsMillis = 60000;

    private int minEvictableIdleTimeMillis = 30000;

    private boolean testWhileIdle = true;

    private boolean testOnBorrow = false;

    private boolean testOnReturn = false;

    @Autowired
    private ResourcePatternResolver resourceResolver;


    @Bean
    public DataSource dataSource() {
        try {
            log.info("Init datasource: url: {}", url);
            dataSource = new DruidDataSource();
            dataSource.setDriverClassName("com.mysql.jdbc.Driver");
            dataSource.setUrl(url);
            dataSource.setUsername(username);
            dataSource.setPassword(password);
            dataSource.setTestWhileIdle(true);
            dataSource.setTestOnReturn(false);
            initDataSource(dataSource);
            dataSource.init();

            transactionManager = new DataSourceTransactionManager(dataSource);
            transactionManager.setDataSource(dataSource);
        } catch (Exception e) {
            log.error("dataSource create error", e);
        }
        log.info("Init done");
        return dataSource;
    }

    @PreDestroy
    public void destroy() {
        try {
            log.info("Close {}", url);
            dataSource.close();
            log.info("Close {} done", url);
        } catch (Throwable t) {
            log.error("Destroy error", t);
        }
    }

    protected void initDataSource(DruidDataSource dataSource) {
        log.info("Data source options: minIdle={}, maxActive={},maxWait={}", minIdle, maxActive, maxWait);
        dataSource.setMinIdle(minIdle);
        dataSource.setMaxActive(maxActive);
        dataSource.setMaxWait(maxWait);
        dataSource.setTimeBetweenEvictionRunsMillis(timeBetweenEvictionRunsMillis);
        dataSource.setMinEvictableIdleTimeMillis(minEvictableIdleTimeMillis);
        dataSource.setTestWhileIdle(testWhileIdle);
        dataSource.setTestOnBorrow(testOnBorrow);
        dataSource.setTestOnReturn(testOnReturn);
        dataSource.setValidationQuery("SELECT 'x'");
    }

    @Bean
    public SqlSessionFactory sqlSessionFactory() throws Exception {
        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        org.apache.ibatis.session.Configuration config = new org.apache.ibatis.session.Configuration();
        config.setMapUnderscoreToCamelCase(true);
        sqlSessionFactoryBean.setConfiguration(config);
        sqlSessionFactoryBean.setDataSource(dataSource());
        List<Resource> resources = new ArrayList<>();
        if (this.mapperLocation != null) {
            try {
                Resource[] mappers = resourceResolver.getResources(mapperLocation);
                resources.addAll(Arrays.asList(mappers));
            } catch (IOException e) {
                log.error("IOException", e);
            }
        }
        Resource[] arr = resources.toArray(new Resource[resources.size()]);
        sqlSessionFactoryBean.setMapperLocations(arr);


        SqlSessionFactory sqlSessionFactory = sqlSessionFactoryBean.getObject();
        sqlSessionFactory.getConfiguration().setMapUnderscoreToCamelCase(true);
        return sqlSessionFactory;
    }

    @Bean
    public DataSourceTransactionManager transactionManager() {
        return transactionManager;
    }
}
