package com.printer.model;

import com.printer.common.UserType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Created by liwei01 on 2018-04-11
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserInfo {

    private Integer id;

    private String username;

    private String password;

    private String phone;

    private String mail;

    private UserType type;

    private Date createTime;

    private Date ts;

    private Integer parentId;

    private Date lastLoginTime;
}
