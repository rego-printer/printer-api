package com.printer.common;

public enum PrintLogType {
    LOGIN("登录"),
    PRINT_NOTIFY("打印请求通知"),
    PRINT_RESULT("打印结果通知"),
    PRINT_STATUS("打印机上报"),
    DISCONNECT("失去连接"),;

    PrintLogType(String message) {
        this.message = message;
    }

    String message;

    public String getMessage() {
        return message;
    }
}
