package com.printer.common;

public enum PrinterTaskStatus {
    CREATE,
    NOTIFIED,
    FINISH,
    CANCEL,
    REPEATTASK,
    OUTOFPAPER,
    CUTERROR,
    OVERHEART,
}
