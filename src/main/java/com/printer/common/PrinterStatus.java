package com.printer.common;

public enum PrinterStatus {
    CREATE,
    ACTIVE,
    DISCONNECT,
    OK,
    REPEATTASK,
    OUTOFPAPER,
    CUTERROR,
    OVERHEART,
    ;
}
