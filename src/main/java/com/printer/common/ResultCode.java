package com.printer.common;

/**
 * Created by liwei01 on 2018-03-27
 */
public enum ResultCode {

    OK(0, "成功"),

    UNKNOWN_EXCEPTION(1, "未知错误"),
    /**
     * <code>ILLEGAL_ARGUMENT = 2;</code>
     */
    ILLEGAL_ARGUMENT(2, "参数错误"),
    /**
     * <code>BAD_REQUEST = 400;</code>
     */
    BAD_REQUEST(400, "错误请求"),
    /**
     * <code>UNAUTHENTICATED = 401;</code>
     */
    UNAUTHENTICATED(401, "未登陆"),
    USER_NOT_EXISTS(1001, "用户不存在"),
    USER_PASSWORD_ERROR(1002, "用户密码错误"),

    ACCESS_USER_ERROR(1003, "用户校验失败"),

    ACCESS_TOKEN_EXPIRE(1004, "access token过期"),

    ACCESS_USER_NO_PERMISSION(1005, "用户没有打印机的权限"),

    ACCESS_BUSINESS_REPEAT(1006, "businessId重复"),
    ACCESS_TASK_NOT_EXISTS(1007, "打印任务不存在"),

    USER_REPEAT(1008, "添加用户错误"),
    PRINTER_REPEAT(1009, "打印机重复"),
    PRINTER_BIND(1010, "打印机已经被绑定"),
    PERMISSION_ERROR(1011, "权限错误"),
    PRINTER_NOT_EXISTS(1012, "打印机不存在"),
    PRINTER_UNBIND(1013, "打印机未被绑定"),;

    private int code;

    private String message;

    ResultCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
