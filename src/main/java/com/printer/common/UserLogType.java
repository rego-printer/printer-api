package com.printer.common;

public enum UserLogType {

    LOGIN("登录"),
    LOGOUT("登出"),
    USER_ADD("添加用户"),
    USER_UPDATE("更新用户"),
    USER_DELETE("删除用户"),
    PRINTER_ADD("添加打印机"),
    PRINTER_UPDATE("更新打印机"),
    PRINTER_DELETE("删除打印机"),
    PRINTER_BIND("绑定打印机"),
    PRINTER_UNBIND("解绑打印机"),
    PRINTER_TASK_ADD("添加打印任务"),
    PRINTER_TASK_CANCEL("取消打印任务"),
    MANUFACTURE_ADD("添加生产任务"),
    MANUFACTURE_UPDATE("更新生产任务"),
    MANUFACTURE_DELETE("删除生产任务"),;

    UserLogType(String message) {
        this.message = message;
    }

    String message;

    public String getMessage() {
        return message;
    }
}
