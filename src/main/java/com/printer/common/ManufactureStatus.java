package com.printer.common;

public enum ManufactureStatus {

    CREATE,
    START,
    FINISH,
    CANCEL,;
}
