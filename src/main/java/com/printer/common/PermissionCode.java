package com.printer.common;

/**
 * Created by liwei01 on 2018-04-10
 */
public enum PermissionCode {
    USER_ADD("添加用户"),
    USER_UPDATE("更新用户"),
    USER_QUERY("查询用户"),
    USER_DELETE("删除用户"),
    PRINTER_QUERY("查询打印机"),
    PRINTER_SELF("我的打印机"),
    PRINTER_ADD("添加打印机"),
    PRINTER_UPDATE("更新打印机"),
    PRINTER_DELETE("删除打印机"),
    PRINTER_BIND("绑定用户"),
    PRINTER_UNBIND("解除绑定"),
    PERMISSION_QUERY("查询权限"),
    PERMISSION_GIVE("赋予权限"),
    PRINTER_TASK_QUERY("打印任务查询"),
    MANUFACTURE_ADD("添加生产"),
    MANUFACTURE_UPDATE("更新生产"),
    MANUFACTURE_QUERY("查询生产"),
    MANUFACTURE_DELETE("删除生产"),
    USER_LOG_QUERY("用户日志查询"),
    USER_LOG_SELF("我的操作日志"),
    PRINTER_LOG_QUERY("打印机日志"),
    USER_PROFILE("个人中心"),
    USER_MESSAGE("用户留言"),
    PRINTER_VERSION("版本管理"),
    PRINTER_CONFIG("配置管理"),
    ;

    String desc;

    PermissionCode(String desc) {
        this.desc = desc;
    }
}
