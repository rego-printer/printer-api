package com.printer.common;

/**
 * Created by liwei01 on 2018-04-11
 */
public enum UserType {
    SYSTEM_ADMIN,
    NORMAL_ADMIN,
    LEVEL_2_AGENT,
    LEVEL_3_AGENT,
    PRODUCER,;
}
